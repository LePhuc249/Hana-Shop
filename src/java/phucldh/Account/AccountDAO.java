/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Account;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class AccountDAO implements Serializable{
    
    public AccountDTO getAccount(String id, String password) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select userID, password, fullname, email, "
                        + "phone, address, statusAccount, roleID From Account "
                        + "Where userID = ? And password = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                stm.setString(2, password);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String idAcc = rs.getString("userID");
                    String pass = rs.getString("password");
                    String fullname = rs.getString("fullname");
                    String email = rs.getString("email");
                    String phone = rs.getString("phone");
                    String address = rs.getString("address");
                    String status = rs.getString("statusAccount");
                    String roleID = rs.getString("roleID");
                    AccountDTO dtoAccount = new AccountDTO( id, password, fullname, email, phone, address, status, roleID);
                    return dtoAccount;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public AccountDTO getAccountByEmail(String emailInput) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT userID, password, fullname, email, phone, address, statusAccount, "
                        + " roleID FROM Account WHERE email = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, emailInput);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String idAcc = rs.getString("userID");
                    String pass = rs.getString("password");
                    String fullname = rs.getString("fullname");
                    String email = rs.getString("email");
                    String phone = rs.getString("phone");
                    String address = rs.getString("address");
                    String status = rs.getString("statusAccount");
                    String roleID = rs.getString("roleID");
                    AccountDTO dtoAccount = new AccountDTO( idAcc, pass, fullname, email, phone, address, status, roleID);
                    return dtoAccount;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public String getRole(String email) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String role = "";
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select roleID From Account Where userID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, email);
                rs = stm.executeQuery();
                if (rs.next()) {
                    role = rs.getString("roleID");
                    return role;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public boolean createAccount(AccountDTO dtoAccount) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Insert into Account (userID, password, fullname, email, phone, "
                        + "address) Values (?, ?, ?, ?, ?, ?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoAccount.getUserID());
                stm.setString(2, dtoAccount.getPasword());
                stm.setString(3, dtoAccount.getFullname());
                stm.setString(4, dtoAccount.getEmail());
                stm.setString(5, dtoAccount.getPhone());
                stm.setString(6, dtoAccount.getAddress());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public boolean updateStatusToActive(String accID) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Account set statusAccount = 'Active' Where userID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, accID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
}
