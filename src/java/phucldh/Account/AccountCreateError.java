/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Account;

/**
 *
 * @author Admin
 */
public class AccountCreateError {

    private String accountIDLengthError;
    private String accountIDTypeErr;
    private String accountIDIsExist;
    private String passwordLengthError;
    private String confirmNotMatch;
    private String fullnameLengthError;
    private String emailLengthError;
    private String emailTypeErr;
    private String phoneLengthErr;
    private String addressLengthErr;

    public AccountCreateError() {
    }

    public AccountCreateError(String accountIDLengthError, String accountIDTypeErr, String accountIDIsExist, String passwordLengthError, String confirmNotMatch, String fullnameLengthError, String emailLengthError, String emailTypeErr, String phoneLengthErr, String addressLengthErr) {
        this.accountIDLengthError = accountIDLengthError;
        this.accountIDTypeErr = accountIDTypeErr;
        this.accountIDIsExist = accountIDIsExist;
        this.passwordLengthError = passwordLengthError;
        this.confirmNotMatch = confirmNotMatch;
        this.fullnameLengthError = fullnameLengthError;
        this.emailLengthError = emailLengthError;
        this.emailTypeErr = emailTypeErr;
        this.phoneLengthErr = phoneLengthErr;
        this.addressLengthErr = addressLengthErr;
    }

    public String getAccountIDLengthError() {
        return accountIDLengthError;
    }

    public void setAccountIDLengthError(String accountIDLengthError) {
        this.accountIDLengthError = accountIDLengthError;
    }

    public String getAccountIDTypeErr() {
        return accountIDTypeErr;
    }

    public void setAccountIDTypeErr(String accountIDTypeErr) {
        this.accountIDTypeErr = accountIDTypeErr;
    }

    public String getAccountIDIsExist() {
        return accountIDIsExist;
    }

    public void setAccountIDIsExist(String accountIDIsExist) {
        this.accountIDIsExist = accountIDIsExist;
    }

    

    public String getPasswordLengthError() {
        return passwordLengthError;
    }

    public void setPasswordLengthError(String passwordLengthError) {
        this.passwordLengthError = passwordLengthError;
    }

    public String getConfirmNotMatch() {
        return confirmNotMatch;
    }

    public void setConfirmNotMatch(String confirmNotMatch) {
        this.confirmNotMatch = confirmNotMatch;
    }

    public String getFullnameLengthError() {
        return fullnameLengthError;
    }

    public void setFullnameLengthError(String fullnameLengthError) {
        this.fullnameLengthError = fullnameLengthError;
    }

    public String getEmailLengthError() {
        return emailLengthError;
    }

    public void setEmailLengthError(String emailLengthError) {
        this.emailLengthError = emailLengthError;
    }

    public String getEmailTypeErr() {
        return emailTypeErr;
    }

    public void setEmailTypeErr(String emailTypeErr) {
        this.emailTypeErr = emailTypeErr;
    }

    public String getPhoneLengthErr() {
        return phoneLengthErr;
    }

    public void setPhoneLengthErr(String phoneLengthErr) {
        this.phoneLengthErr = phoneLengthErr;
    }

    public String getAddressLengthErr() {
        return addressLengthErr;
    }

    public void setAddressLengthErr(String addressLengthErr) {
        this.addressLengthErr = addressLengthErr;
    }

}
