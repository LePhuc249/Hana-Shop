/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Account;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class AccountDTO implements Serializable{
    private String userID;
    private String pasword;
    private String fullname;
    private String email;
    private String phone;
    private String address;
    private Date createDate;
    private String status;
    private String role;

    public AccountDTO() {
    }

    public AccountDTO(String userID, String pasword) {
        this.userID = userID;
        this.pasword = pasword;
    }

    public AccountDTO(String userID, String pasword, String fullname, String email, String phone, String address) {
        this.userID = userID;
        this.pasword = pasword;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }


    public AccountDTO(String userID, String pasword, String fullname, String email, String phone, String address, String status) {
        this.userID = userID;
        this.pasword = pasword;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.status = status;
    }

    public AccountDTO(String userID, String pasword, String fullname, String email, String phone, String address, String status, String role) {
        this.userID = userID;
        this.pasword = pasword;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.status = status;
        this.role = role;
    }

    public AccountDTO(String userID, String pasword, String fullname, String email, String phone, String address, Date createDate, String status, String role) {
        this.userID = userID;
        this.pasword = pasword;
        this.fullname = fullname;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.createDate = createDate;
        this.status = status;
        this.role = role;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
