/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.OrderDetail;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class OrderDetailDTO implements Serializable{
    private String orderLineID;
    private String orderID;
    private String itemID;
    private int amount;
    private float price;
    private String itemProperty;
    private Date createDate;

    public OrderDetailDTO(String orderLineID, String orderID, String itemID, int amount, float price, String itemProperty) {
        this.orderLineID = orderLineID;
        this.orderID = orderID;
        this.itemID = itemID;
        this.amount = amount;
        this.price = price;
        this.itemProperty = itemProperty;
    }

    public OrderDetailDTO(String orderLineID, String orderID, String itemID, int amount, float price, String itemProperty, Date createDate) {
        this.orderLineID = orderLineID;
        this.orderID = orderID;
        this.itemID = itemID;
        this.amount = amount;
        this.price = price;
        this.itemProperty = itemProperty;
        this.createDate = createDate;
    }

    public String getOrderLineID() {
        return orderLineID;
    }

    public void setOrderLineID(String orderLineID) {
        this.orderLineID = orderLineID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getItemProperty() {
        return itemProperty;
    }

    public void setItemProperty(String itemProperty) {
        this.itemProperty = itemProperty;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

}
