/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.OrderDetail;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class OrderDetailDAO implements Serializable {

    private List<OrderDetailDTO> listItem;

    public List<OrderDetailDTO> ListItemInOrder() throws NamingException, SQLException {
        return listItem;
    }
    
    private List<String> listProperty;
    
    public List<String> ListProperty() throws NamingException, SQLException {
        return listProperty;
    }

    public void getItem(String orderIDGet) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID, orderID, itemID, amount, price, itemProperty FROM OrderDetail Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderIDGet);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String lineID = rs.getString("orderLineID");
                    String orderID = rs.getString("orderID");
                    String itemID = rs.getString("itemID");
                    int amount = rs.getInt("amount");
                    float price = rs.getFloat("price");
                    String property = rs.getString("itemProperty");
                    OrderDetailDTO dto = new OrderDetailDTO(lineID, orderID, itemID, amount, price, property);
                    if (this.listItem == null) {
                        this.listItem = new ArrayList<>();
                    }
                    this.listItem.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void getListProperty(String orderIDGet) throws NamingException, SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemProperty FROM OrderDetail Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderIDGet);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String property = rs.getString("itemProperty");
                    if (this.listProperty == null) {
                        this.listProperty = new ArrayList<>();
                    }
                    this.listProperty.add(property);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean createOrderDetail(OrderDetailDTO dtoOrderDetail) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO OrderDetail (orderLineID, orderID, itemID, amount, price, itemProperty) "
                        + "VALUES (?,?,?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoOrderDetail.getOrderLineID());
                stm.setString(2, dtoOrderDetail.getOrderID());
                stm.setString(3, dtoOrderDetail.getItemID());
                stm.setInt(4, dtoOrderDetail.getAmount());
                stm.setFloat(5, dtoOrderDetail.getPrice());
                stm.setString(6, dtoOrderDetail.getItemProperty());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public OrderDetailDTO getUserOrderDetail(String idOfDetail) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID, orderID, itemID, amount, price, itemProperty FROM OrderDetail Where orderLineID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, idOfDetail);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String lineID = rs.getString("orderLineID");
                    String orderID = rs.getString("orderID");
                    String itemID = rs.getString("itemID");
                    int amount = rs.getInt("amount");
                    float price = rs.getFloat("price");
                    String property = rs.getString("itemProperty");
                    OrderDetailDTO dto = new OrderDetailDTO(lineID, orderID, itemID, amount, price, property);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public String getLastOrderDetailByIDByDate() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String id = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderLineID FROM OrderDetail WHERE createDate = (SELECT MAX(createDate) FROM OrderDetail) ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    id = rs.getString("orderLineID");
                }
                return id;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
}
