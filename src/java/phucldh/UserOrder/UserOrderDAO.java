/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrder;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class UserOrderDAO implements Serializable {

    private List<UserOrderDTO> listAllUserOrder;

    public List<UserOrderDTO> ListAllOrder() throws NamingException, SQLException {
        return listAllUserOrder;
    }

    private List<UserOrderDTO> listOrderOfUser;

    public List<UserOrderDTO> ListOrderOfUser() throws NamingException, SQLException {
        return listOrderOfUser;
    }

    private List<String> listCouponsID;

    public List<String> ListCouponsHaveUsed() throws NamingException, SQLException {
        return listCouponsID;
    }

    private List<UserOrderDTO> listSearch;

    public List<UserOrderDTO> ListSearch() throws NamingException, SQLException {
        return listSearch;
    }

    public void getAllListOrder() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, customerID, dateDelivery, paymentMehtod, "
                        + "status, totalPrice, couponID FROM UserOrder";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    String customerID = rs.getString("customerID");
                    String dateDelivery = rs.getString("dateDelivery");
                    String paymentMehtod = rs.getString("paymentMehtod");
                    float totalPrice = rs.getFloat("totalPrice");
                    String status = rs.getString("status");
                    String couponID = rs.getString("couponID");
                    UserOrderDTO dto = new UserOrderDTO(id, customerID, dateDelivery, paymentMehtod, totalPrice, status, couponID);
                    if (this.listAllUserOrder == null) {
                        this.listAllUserOrder = new ArrayList<>();
                    }
                    this.listAllUserOrder.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void getListOrderOfUser(String idCus) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, customerID, dateDelivery, paymentMehtod, "
                        + "status, totalPrice, couponID FROM UserOrder "
                        + "Where customerID = ? Order by createDate DESC";
                stm = conn.prepareStatement(sql);
                stm.setString(1, idCus);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    String customerID = rs.getString("customerID");
                    String dateDelivery = rs.getString("dateDelivery");
                    String paymentMehtod = rs.getString("paymentMehtod");
                    float totalPrice = rs.getFloat("totalPrice");
                    String status = rs.getString("status");
                    String couponID = rs.getString("couponID");
                    UserOrderDTO dto = new UserOrderDTO(id, customerID, dateDelivery, paymentMehtod, totalPrice, status, couponID);
                    if (this.listOrderOfUser == null) {
                        this.listOrderOfUser = new ArrayList<>();
                    }
                    this.listOrderOfUser.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void getSearchByNameOrDate(String date, String name, String userID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT uo.orderID, uo.createDate, uo.dateDelivery, uo.paymentMehtod, "
                        + "uo.totalPrice,uo.status, uo.couponID, od.itemProperty "
                        + "FROM dbo.UserOrder uo, dbo.OrderDetail od "
                        + "WHERE CONVERT(char(10), uo.createDate,126) = ? AND od.itemProperty = ? "
                        + "AND uo.customerID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, date);
                stm.setString(2, name );
                stm.setString(3, userID);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    Date createDate = rs.getDate("createDate");
                    String dateDelivery = rs.getString("dateDelivery");
                    String paymentMehtod = rs.getString("paymentMehtod");
                    float totalPrice = rs.getFloat("totalPrice");
                    String status = rs.getString("status");
                    String couponID = rs.getString("couponID");
                    String property = rs.getString("itemProperty");
                    UserOrderDTO dto = new UserOrderDTO(id, createDate, dateDelivery, paymentMehtod, totalPrice, status, couponID, property);
                    if (this.listSearch == null) {
                        this.listSearch = new ArrayList<>();
                    }
                    this.listSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void getSearchByName(String name, String userID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT uo.orderID, uo.createDate, uo.dateDelivery, uo.paymentMehtod, "
                        + "uo.totalPrice,uo.status, uo.couponID, od.itemProperty "
                        + "FROM dbo.UserOrder uo, dbo.OrderDetail od "
                        + "WHERE od.itemProperty = ? AND uo.customerID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, name);
                stm.setString(2, userID);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    Date createDate = rs.getDate("createDate");
                    String dateDelivery = rs.getString("dateDelivery");
                    String paymentMehtod = rs.getString("paymentMehtod");
                    float totalPrice = rs.getFloat("totalPrice");
                    String status = rs.getString("status");
                    String couponID = rs.getString("couponID");
                    String property = rs.getString("itemProperty");
                    UserOrderDTO dto = new UserOrderDTO(id, createDate, dateDelivery, paymentMehtod, totalPrice, status, couponID, property);
                    if (this.listSearch == null) {
                        this.listSearch = new ArrayList<>();
                    }
                    this.listSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void getSearchByDate(String date, String userID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT uo.orderID, uo.createDate, uo.dateDelivery, uo.paymentMehtod, "
                        + "uo.totalPrice,uo.status, uo.couponID, od.itemProperty "
                        + "FROM dbo.UserOrder uo, dbo.OrderDetail od "
                        + "WHERE CONVERT(char(10), uo.createDate,126) = ? AND uo.customerID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, date);
                stm.setString(2, userID);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("orderID");
                    Date createDate = rs.getDate("createDate");
                    String dateDelivery = rs.getString("dateDelivery");
                    String paymentMehtod = rs.getString("paymentMehtod");
                    float totalPrice = rs.getFloat("totalPrice");
                    String status = rs.getString("status");
                    String couponID = rs.getString("couponID");
                    String property = rs.getString("itemProperty");
                    UserOrderDTO dto = new UserOrderDTO(id, createDate, dateDelivery, paymentMehtod, totalPrice, status, couponID, property);
                    if (this.listSearch == null) {
                        this.listSearch = new ArrayList<>();
                    }
                    this.listSearch.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean updateStatusToFinish(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Fi' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateStatusToCancel(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Ca' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateStatusToConfirm(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update UserOrder SET status = 'Co' WHERE orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean createNewOrder(UserOrderDTO dtoOrder) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO UserOrder "
                        + "(orderID, customerID, dateDelivery, paymentMehtod, "
                        + "totalPrice, status,  couponID) VALUES (?,?,?,?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoOrder.getOrderID());
                stm.setString(2, dtoOrder.getCustomerID());
                stm.setString(3, dtoOrder.getDateDelivery());
                stm.setString(4, dtoOrder.getPaymentMehtod());
                stm.setFloat(5, dtoOrder.getTotalPrice());
                stm.setString(6, dtoOrder.getStatus());
                stm.setString(7, dtoOrder.getCouponID());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean createNewOrderWithOutCoupons(UserOrderDTO dtoOrder) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "INSERT INTO UserOrder "
                        + "(orderID, customerID, dateDelivery, paymentMehtod, "
                        + "totalPrice, status) VALUES (?,?,?,?,?,?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoOrder.getOrderID());
                stm.setString(2, dtoOrder.getCustomerID());
                stm.setString(3, dtoOrder.getDateDelivery());
                stm.setString(4, dtoOrder.getPaymentMehtod());
                stm.setFloat(5, dtoOrder.getTotalPrice());
                stm.setString(6, dtoOrder.getStatus());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public String getStatusOrder(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String status = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT status FROM UserOrder Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    status = rs.getString("status");
                }
                return status;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return status;
    }

    public UserOrderDTO getOrder(String orderID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        UserOrderDTO dto = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID, customerID, createDate, dateDelivery, paymentMehtod, "
                        + "status, totalPrice, couponID FROM UserOrder Where orderID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, orderID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("orderID");
                    String customerID = rs.getString("customerID");
                    Date createDate = rs.getDate("createDate");
                    String dateDelivery = rs.getString("dateDelivery");
                    String paymentMehtod = rs.getString("paymentMehtod");
                    float totalPrice = rs.getFloat("totalPrice");
                    String status = rs.getString("status");
                    String couponID = rs.getString("couponID");
                    dto = new UserOrderDTO(id, customerID, createDate, dateDelivery, paymentMehtod, totalPrice, status, couponID);
                }
                return dto;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public void getListCoupons() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT couponID FROM UserOrder";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("couponID");
                    if (this.listCouponsID == null) {
                        this.listCouponsID = new ArrayList<>();
                    }
                    this.listCouponsID.add(id);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String getLastOrderByIDByDate() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String id = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT orderID FROM UserOrder WHERE createDate = (SELECT MAX(createDate) FROM UserOrder)";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    id = rs.getString("orderID");
                }
                return id;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

}
