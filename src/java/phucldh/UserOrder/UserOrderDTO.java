/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.UserOrder;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class UserOrderDTO implements Serializable {

    private String orderID;
    private String customerID;
    private Date createDate;
    private String dateDelivery;
    private String paymentMehtod;
    private float totalPrice;
    private String status;
    private String couponID;
    private String itemInDetail;

    public UserOrderDTO() {
    }
    
    public UserOrderDTO(String orderID, String customerID, String dateDelivery, String paymentMehtod, float totalPrice, String status) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.dateDelivery = dateDelivery;
        this.paymentMehtod = paymentMehtod;
        this.totalPrice = totalPrice;
        this.status = status;
    }
    
    public UserOrderDTO(String orderID, String customerID, String dateDelivery, String paymentMehtod, float totalPrice, String status, String couponID) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.dateDelivery = dateDelivery;
        this.paymentMehtod = paymentMehtod;
        this.totalPrice = totalPrice;
        this.status = status;
        this.couponID = couponID;
    }

    public UserOrderDTO(String orderID, Date createDate, String dateDelivery, String paymentMehtod, float totalPrice, String status, String couponID, String itemInDetail) {
        this.orderID = orderID;
        this.createDate = createDate;
        this.dateDelivery = dateDelivery;
        this.paymentMehtod = paymentMehtod;
        this.totalPrice = totalPrice;
        this.status = status;
        this.couponID = couponID;
        this.itemInDetail = itemInDetail;
    }

    public UserOrderDTO(String orderID, String customerID, Date createDate, String dateDelivery, String paymentMehtod, float totalPrice, String status, String couponID) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.createDate = createDate;
        this.dateDelivery = dateDelivery;
        this.paymentMehtod = paymentMehtod;
        this.totalPrice = totalPrice;
        this.status = status;
        this.couponID = couponID;
    }

    public String getItemInDetail() {
        return itemInDetail;
    }

    public void setItemInDetail(String itemInDetail) {
        this.itemInDetail = itemInDetail;
    }    
    
    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(String dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public String getPaymentMehtod() {
        return paymentMehtod;
    }

    public void setPaymentMehtod(String paymentMehtod) {
        this.paymentMehtod = paymentMehtod;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCouponID() {
        return couponID;
    }

    public void setCouponID(String couponID) {
        this.couponID = couponID;
    }

}
