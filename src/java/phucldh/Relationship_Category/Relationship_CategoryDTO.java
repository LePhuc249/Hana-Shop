/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Relationship_Category;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class Relationship_CategoryDTO implements Serializable {

    private String RelationshipId;
    private String categoryID1;
    private String categoryID2;

    public Relationship_CategoryDTO() {
    }

    public Relationship_CategoryDTO(String RelationshipId, String categoryID1, String categoryID2) {
        this.RelationshipId = RelationshipId;
        this.categoryID1 = categoryID1;
        this.categoryID2 = categoryID2;
    }

    public String getRelationshipId() {
        return RelationshipId;
    }

    public void setRelationshipId(String RelationshipId) {
        this.RelationshipId = RelationshipId;
    }

    public String getCategoryID1() {
        return categoryID1;
    }

    public void setCategoryID1(String categoryID1) {
        this.categoryID1 = categoryID1;
    }

    public String getCategoryID2() {
        return categoryID2;
    }

    public void setCategoryID2(String categoryID2) {
        this.categoryID2 = categoryID2;
    }

}
