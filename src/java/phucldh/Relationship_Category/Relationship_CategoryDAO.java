/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Relationship_Category;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class Relationship_CategoryDAO implements Serializable{
    
    public Relationship_CategoryDTO getRelation(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Select RelationshipId, categoryID1, categoryID2 From "
                        + "Relationship_Category Where categoryID1 = ? OR categoryID2 = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setString(2, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("RelationshipId");
                    String cate1 = rs.getString("categoryID1");
                    String cate2 = rs.getString("categoryID2");
                    Relationship_CategoryDTO dto = new Relationship_CategoryDTO(id, cate1, cate2);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
}
