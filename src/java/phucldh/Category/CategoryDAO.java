/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Category;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class CategoryDAO implements Serializable{
    
    private List<CategoryDTO> listAllCategory;

    public List<CategoryDTO> getListAllCategory() throws NamingException, SQLException {
        return listAllCategory;
    }
    
    public void getFullCategory() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT categoryID, categoryName FROM Category Where isDeleted = 0";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("categoryID");
                    String name = rs.getString("categoryName");
                    CategoryDTO dto = new CategoryDTO(id, name);
                    if (this.listAllCategory == null) {
                        this.listAllCategory = new ArrayList<>();
                    }
                    this.listAllCategory.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
