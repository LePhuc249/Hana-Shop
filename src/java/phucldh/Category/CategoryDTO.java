/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Category;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class CategoryDTO implements Serializable {

    private String CategoryID;
    private String CategoryName;
    private Date createDate;
    private String createBy;
    private boolean isDeleted;

    public CategoryDTO() {
    }

    public CategoryDTO(String CategoryID, String CategoryName) {
        this.CategoryID = CategoryID;
        this.CategoryName = CategoryName;
    }

    public CategoryDTO(String CategoryID, String CategoryName, boolean isDeleted) {
        this.CategoryID = CategoryID;
        this.CategoryName = CategoryName;
        this.isDeleted = isDeleted;
    }

    public CategoryDTO(String CategoryID, String CategoryName, Date createDate, String createBy, boolean isDeleted) {
        this.CategoryID = CategoryID;
        this.CategoryName = CategoryName;
        this.createDate = createDate;
        this.createBy = createBy;
        this.isDeleted = isDeleted;
    }

    public String getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(String CategoryID) {
        this.CategoryID = CategoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String CategoryName) {
        this.CategoryName = CategoryName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
