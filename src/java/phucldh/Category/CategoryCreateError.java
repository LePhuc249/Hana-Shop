/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Category;

/**
 *
 * @author Admin
 */
public class CategoryCreateError {

    private String categoryIdLengthError;
    private String categoryIdHaveBeenExistError;
    private String categoryNameLengthError;

    public CategoryCreateError() {
    }

    public CategoryCreateError(String categoryIdLengthError, String categoryIdHaveBeenExistError, String categoryNameLengthError) {
        this.categoryIdLengthError = categoryIdLengthError;
        this.categoryIdHaveBeenExistError = categoryIdHaveBeenExistError;
        this.categoryNameLengthError = categoryNameLengthError;
    }

    public String getCategoryIdHaveBeenExistError() {
        return categoryIdHaveBeenExistError;
    }

    public void setCategoryIdHaveBeenExistError(String categoryIdHaveBeenExistError) {
        this.categoryIdHaveBeenExistError = categoryIdHaveBeenExistError;
    }

    public String getCategoryIdLengthError() {
        return categoryIdLengthError;
    }

    public void setCategoryIdLengthError(String categoryIdLengthError) {
        this.categoryIdLengthError = categoryIdLengthError;
    }

    public String getCategoryNameLengthError() {
        return categoryNameLengthError;
    }

    public void setCategoryNameLengthError(String categoryNameLengthError) {
        this.categoryNameLengthError = categoryNameLengthError;
    }
}
