/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Feedback;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class FeedbackDTO implements Serializable{
    private String FeedbackID;
    private String accountID;
    private String orderLineID;
    private Date feedbackDate;
    private String feedbackContent;
    private int counter;
    private String status;

    public FeedbackDTO() {
    }

    public FeedbackDTO(String FeedbackID, String accountID, String orderLineID, String feedbackContent, int counter, String status) {
        this.FeedbackID = FeedbackID;
        this.accountID = accountID;
        this.orderLineID = orderLineID;
        this.feedbackContent = feedbackContent;
        this.counter = counter;
        this.status = status;
    }

    public FeedbackDTO(String FeedbackID, String accountID, String orderLineID, Date feedbackDate, String feedbackContent, int counter, String status) {
        this.FeedbackID = FeedbackID;
        this.accountID = accountID;
        this.orderLineID = orderLineID;
        this.feedbackDate = feedbackDate;
        this.feedbackContent = feedbackContent;
        this.counter = counter;
        this.status = status;
    }

    public String getFeedbackID() {
        return FeedbackID;
    }

    public void setFeedbackID(String FeedbackID) {
        this.FeedbackID = FeedbackID;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getOrderLineID() {
        return orderLineID;
    }

    public void setOrderLineID(String orderLineID) {
        this.orderLineID = orderLineID;
    }

    public Date getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(Date feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeedbackContent() {
        return feedbackContent;
    }

    public void setFeedbackContent(String feedbackContent) {
        this.feedbackContent = feedbackContent;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
