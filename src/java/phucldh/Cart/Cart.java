/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Cart;

import java.io.Serializable;
import java.util.HashMap;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;

/**
 *
 * @author Admin
 */
public class Cart implements Serializable {

    private String customerName;
    private HashMap<String, FoodOrDrinkDTO> shoppingCart;

    public Cart() {
        this.customerName = "Guest";
        this.shoppingCart = new HashMap<>();
    }

    public Cart(String customerNameO) {
        this.customerName = customerName;
        this.shoppingCart = new HashMap<>();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public HashMap<String, FoodOrDrinkDTO> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(HashMap<String, FoodOrDrinkDTO> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public void addToCart(FoodOrDrinkDTO item) {
        if (this.shoppingCart.containsKey(item.getItemID())) {
            int quantity = this.shoppingCart.get(item.getItemID()).getQuantity() + 1;
            item.setQuantity(quantity);
        }
        this.shoppingCart.put(item.getItemID(), item);
    }

    public void update(String id, int quantity) {
        if (this.shoppingCart.containsKey(id)) {
            this.shoppingCart.get(id).setQuantity(quantity);
        }
    }

    public void remove(String id) {
        if (this.shoppingCart.containsKey(id)) {
            this.shoppingCart.remove(id);
        }
    }

    public float getTotal() {
        float total = 0;
        for (FoodOrDrinkDTO item : this.shoppingCart.values()) {
            total += item.getPrice() * item.getQuantity();
        }
        return total;
    }
}
