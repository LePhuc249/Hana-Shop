/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.OrderDetail.OrderDetailDAO;
import phucldh.OrderDetail.OrderDetailDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "DeleteOrderServlet", urlPatterns = {"/DeleteOrderServlet"})
public class DeleteOrderServlet extends HttpServlet {

    private final String HISTORY_PAGE = "showHistoryPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final int RECORD_PER_PAGE = 20;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = HISTORY_PAGE;
        String errorContent = "There having the error to delete order please check it again";

        boolean error = false;

        try {
            ServletContext context = getServletContext();

            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            UserOrderDAO daoUserOrder = new UserOrderDAO();
            OrderDetailDAO daoOrderDetail = new OrderDetailDAO();
            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");

            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORDELETE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                String id = (String) request.getAttribute("ORDERIDDELETE");
                                String dateSearch = (String) request.getAttribute("DATESEARCHHISTORY");
                                String nameSearch = (String) request.getAttribute("NAMESEARCHHISTORY");
                                UserOrderDTO dtoUserOrder = daoUserOrder.getOrder(id);
                                if (dtoUserOrder != null) {
                                    String orderStatus = dtoUserOrder.getStatus();
                                    if (orderStatus.equalsIgnoreCase("Fi")) {
                                        error = true;
                                        errorContent = "The current status is finish so you can't delete order please wait uttil order finish";
                                    } else if (orderStatus.equalsIgnoreCase("Ca")) {
                                        error = true;
                                        errorContent = "The current status is cancel so you can't delete order";
                                    } else if (orderStatus.equalsIgnoreCase("Pr") || orderStatus.equalsIgnoreCase("Co")) {
                                        boolean updateStatus = daoUserOrder.updateStatusToCancel(id);
                                        if (updateStatus) {
                                            daoOrderDetail.getItem(dtoUserOrder.getOrderID());
                                            List<OrderDetailDTO> listOrderDetail = daoOrderDetail.ListItemInOrder();
                                            for (OrderDetailDTO orderDetailDTO : listOrderDetail) {
                                                String itemID = orderDetailDTO.getItemID();
                                                int amount = orderDetailDTO.getAmount();
                                                int currentAmount = daoFoodOrDrink.getTotalQuantityItemByID(itemID);
                                                int updateAmount = currentAmount + amount;
                                                boolean updateQuantity = daoFoodOrDrink.updateQuantityItem(itemID, updateAmount);
                                                if (updateQuantity) {
                                                    url = "SearchHistoryServlet?txtShoppingDate=" + dateSearch + "&txtSearchName=" + nameSearch
                                                            + "&btAction=SearchHistoryBuy";
                                                }
                                            }
                                        }
                                    }
                                    if (error) {
                                        request.setAttribute("ERRORDELETEORDER", errorContent);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("DeleteOrderServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("DeleteOrderServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("DeleteOrderServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
