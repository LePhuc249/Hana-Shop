/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Feedback.FeedbackDAO;
import phucldh.Feedback.FeedbackDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SubmitFeedbackServlet", urlPatterns = {"/SubmitFeedbackServlet"})
public class SubmitFeedbackServlet extends HttpServlet {

    private final String HISTORY_PAGE = "showHistoryPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String FEEDBACK_PAGE = "feedbackPage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = FEEDBACK_PAGE;
        String orderID = request.getParameter("txtOrderID");
        String comment = request.getParameter("txtComment");
        String quanlity = request.getParameter("txtNumberQuality");
        String errorContent = "There having error when send feedback please try again";

        boolean error = false;

        try {
            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            FeedbackDAO daoFeedback = new FeedbackDAO();

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");

            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                String cusId = dtoAccount.getUserID();
                                String lastFeedId = daoFeedback.getLastFeedbackID();
                                String feedId = null;
                                if (lastFeedId == null) {
                                    feedId = "F_1";
                                } else if (lastFeedId != null) {
                                    String numberString = lastFeedId.substring(2);
                                    int number = Integer.parseInt(numberString);
                                    number = number + 1;
                                    feedId = "F_" + number;
                                }
                                int quanlityNumber = Integer.parseInt(quanlity);
                                if (quanlityNumber < 0 || quanlityNumber > 10) {
                                    error = true;
                                    errorContent = "The score must between 1 to 10";
                                }
                                if (error) {
                                    request.setAttribute("ERRORSENDFEEDBACK", errorContent);
                                } else {
                                    FeedbackDTO dtoFeedback = new FeedbackDTO(feedId, cusId, orderID, comment, quanlityNumber, "Submited");
                                    boolean createFeedback = daoFeedback.createFeedback(dtoFeedback);
                                    if (createFeedback) {
                                        url = HISTORY_PAGE;
                                        session.setAttribute("ORDER", null);
                                        session.setAttribute("ORDERDETAILLIST", null);
                                        session.setAttribute("COUPONSLIST", null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("SubmitFeedbackServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("SubmitFeedbackServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("SubmitFeedbackServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("SubmitFeedbackServlet_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
