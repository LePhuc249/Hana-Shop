/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Cart.Cart;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.Google.GooglePojo;
import phucldh.Google.GoogleUtils;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "LoginGoogleServlet", urlPatterns = {"/LoginGoogleServlet"})
public class LoginGoogleServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private final String INVALID_LOGIN_PAGE = "invalidLoginGoogle";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final int RECORD_PER_PAGE = 20;
    private final String VERIFY_PAGE = "verifyCodePage";

    public LoginGoogleServlet() {
        super();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int page = 1;
        int noOfRecord = 0;

        String url = INVALID_LOGIN_PAGE;
        String code = request.getParameter("code");

        try {
            ServletContext context = getServletContext();

            HttpSession session = request.getSession(true);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();

            if (code == null || code.isEmpty()) {
                url = "LoginPage";
            } else {
                String accessToken = GoogleUtils.getToken(code);
                if (true) {
                    GooglePojo googlePojo = GoogleUtils.getUserInfo(accessToken);
                    if (googlePojo != null) {
                        String emailAccount = googlePojo.getEmail();
                        if (emailAccount != null) {
                            AccountDTO dtoAccount = daoAccount.getAccountByEmail(emailAccount);
                            if (dtoAccount != null) {
                                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                                if (roleID != null && !roleID.equals("")) {
                                    String roleName = daoRole.getRoleName(roleID);
                                    if (roleName != null && !roleName.equals("")) {
                                        if (roleName.equalsIgnoreCase("Admin")) {
                                            url = ADMIN_PAGE;

                                            context.setAttribute("ITEMLIST", null);
                                            List<FoodOrDrinkDTO> listItem = null;
                                            daoFoodOrDrink.getAllItemForAdmin((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                            listItem = daoFoodOrDrink.getListAllFoodOrDrink();
                                            context.setAttribute("ITEMLIST", listItem);

                                            noOfRecord = daoFoodOrDrink.countFoodForAdmin();
                                            if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                                                int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                                                session.setAttribute("NOOFPAGE", noOfPage);
                                                session.setAttribute("CURRENTPAGE", page);
                                            }
                                        } else if (roleName.equalsIgnoreCase("Customer")) {
                                            session.setAttribute("STATUSACCOUNT", dtoAccount.getStatus());
                                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                                url = VERIFY_PAGE;
                                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                                url = CUSTOMER_PAGE;
                                                FoodOrDrinkDTO dtoFoodOrDrink = (FoodOrDrinkDTO) session.getAttribute("FOODTOADD");
                                                if (dtoFoodOrDrink != null) {
                                                    Cart shoppingCart = new Cart(dtoAccount.getUserID());
                                                    shoppingCart.addToCart(dtoFoodOrDrink);
                                                    session.setAttribute("shoppingCart", shoppingCart);
                                                    session.setAttribute("FOODTOADD", null);
                                                }
                                            }
                                        }
                                        Cookie usernameCookie = new Cookie("USERID", dtoAccount.getUserID());
                                        usernameCookie.setMaxAge(60 * 3);
                                        response.addCookie(usernameCookie);
                                        Cookie passwordCookie = new Cookie("PASSWORD", dtoAccount.getPasword());
                                        passwordCookie.setMaxAge(60 * 3);
                                        response.addCookie(passwordCookie);
                                        session.setAttribute("ACCOUNTOBJECT", dtoAccount);
                                        session.setAttribute("ROLENAME", roleName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("LoginGoogleServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("LoginGoogleServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("LoginGoogleServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("LoginGoogleServlet_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
