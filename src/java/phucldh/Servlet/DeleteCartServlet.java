/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Cart.Cart;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "DeleteCartServlet", urlPatterns = {"/DeleteCartServlet"})
public class DeleteCartServlet extends HttpServlet {

    private final String VIEW_CART_PAGE = "viewCartPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = VIEW_CART_PAGE;
        String errorContent = "There having error to delete item in cart please try it again";

        boolean error = false;

        try {
            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");

            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORROLE", null);
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                Cart shoppingCart = (Cart) session.getAttribute("shoppingCart");
                                if (shoppingCart != null) {
                                    List<String> listID = (List) request.getAttribute("LISTDELETED");
                                    if (listID != null) {
                                        int count = 0;
                                        for (String id : listID) {
                                            shoppingCart.remove(id);
                                            count++;
                                        }
                                        if (count == listID.size()) {
                                            session.setAttribute("shoppingCart", shoppingCart);
                                        } else {
                                            error = true;
                                        }
                                        if (error) {
                                            request.setAttribute("ERRORDELETEITEMINCART", errorContent);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("DeleteCartServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("DeleteCartServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("DeleteCartServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
