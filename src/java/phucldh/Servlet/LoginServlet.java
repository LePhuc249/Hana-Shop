/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Cart.Cart;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.Google.VerifyRecaptcha;
import phucldh.Mail.SendEmail;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    private final int RECORD_PER_PAGE = 20;
    private final String INVALID_LOGIN = "invalidLoginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String VERIFY_PAGE = "verifyCodePage";
    private final String INVALID_RECAPTCHA = "invalidReCaptchaPage";
    private final String SECURE_HASH_ALGORITHM = "SHA-256";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int page = 1;
        int noOfRecord = 0;

        String url = INVALID_LOGIN;
        String userID = request.getParameter("txtUserID");
        String password = request.getParameter("txtPassword");
        String pageParam = request.getParameter("page");

        try {
            ServletContext context = getServletContext();

            HttpSession session = request.getSession(true);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();

            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }

            MessageDigest md = MessageDigest.getInstance(SECURE_HASH_ALGORITHM);
            byte[] digest = md.digest(password.getBytes(StandardCharsets.UTF_8));
            String passwordEncrypte = DatatypeConverter.printHexBinary(digest).toLowerCase();
            AccountDTO dtoAccount = daoAccount.getAccount(userID, passwordEncrypte);
            if (dtoAccount != null) {
                String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
                boolean verify = VerifyRecaptcha.verify(gRecaptchaResponse);
                if (verify) {
                    String roleID = daoAccount.getRole(dtoAccount.getUserID());
                    if (roleID != null && !roleID.equals("")) {
                        String roleName = daoRole.getRoleName(roleID);
                        if (roleName != null && !roleName.equals("")) {
                            if (roleName.equalsIgnoreCase("Admin")) {
                                url = ADMIN_PAGE;

                                context.setAttribute("ITEMLISTFORADMIN", null);
                                List<FoodOrDrinkDTO> listItem = null;
                                daoFoodOrDrink.getAllItemForAdmin((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                listItem = daoFoodOrDrink.getListAllFoodOrDrink();
                                context.setAttribute("ITEMLISTFORADMIN", listItem);

                                noOfRecord = daoFoodOrDrink.countFoodForAdmin();
                                if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                                    int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                                    session.setAttribute("NOOFPAGEFORADMIN", noOfPage);
                                    session.setAttribute("CURRENTPAGEFORADMIN", page);
                                }
                            } else if (roleName.equalsIgnoreCase("Customer")) {
                                session.setAttribute("STATUSACCOUNT", dtoAccount.getStatus());
                                if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                    url = VERIFY_PAGE;
                                    String codeVerify = (String) session.getAttribute("VERIFYCODE");
                                    if (codeVerify == null) {
                                        SendEmail sm = new SendEmail();
                                        String codeVerifyNew = sm.getCodeRandom();
                                        if (codeVerifyNew != null) {
                                            boolean send = sm.sendEmail(dtoAccount, codeVerifyNew);
                                            if (send) {
                                                request.setAttribute("ERRORVERIFY", "Old code active have been delete please check email to get the new code");
                                                session.setAttribute("VERIFYCODE", codeVerifyNew);
                                            }
                                        }
                                    }
                                } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                    url = CUSTOMER_PAGE;
                                    FoodOrDrinkDTO dtoFoodOrDrink = (FoodOrDrinkDTO) session.getAttribute("FOODTOADD");
                                    if (dtoFoodOrDrink != null) {
                                        Cart shoppingCart = new Cart(dtoAccount.getUserID());
                                        shoppingCart.addToCart(dtoFoodOrDrink);
                                        session.setAttribute("shoppingCart", shoppingCart);
                                        session.setAttribute("FOODTOADD", null);
                                    }
                                }
                            }
                            Cookie usernameCookie = new Cookie("USERID", userID);
                            usernameCookie.setMaxAge(60 * 3);
                            response.addCookie(usernameCookie);
                            Cookie passwordCookie = new Cookie("PASSWORD", password);
                            passwordCookie.setMaxAge(60 * 3);
                            response.addCookie(passwordCookie);
                            session.setAttribute("ACCOUNTOBJECT", dtoAccount);
                            session.setAttribute("ROLENAME", roleName);
                        }
                    }
                } else {
                    url = INVALID_RECAPTCHA;
                }
            }
        } catch (NamingException ex) {
            log("LoginServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("LoginServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("LoginServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("LoginServlet_NumberFormat " + ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            log("LoginServlet_NoSuchAlgorithm " + ex.getMessage());
        } catch (MessagingException ex) {
            log("LoginServlet_Messaging " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
