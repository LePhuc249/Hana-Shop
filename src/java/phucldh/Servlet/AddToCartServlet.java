/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Cart.Cart;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "AddToCartServlet", urlPatterns = {"/AddToCartServlet"})
public class AddToCartServlet extends HttpServlet {

    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int page = 1;

        String url = CUSTOMER_PAGE;
        String pageParam = request.getParameter("page");
        String lastSearchValue = (String) request.getParameter("txtSearchItem");
        if (lastSearchValue == null) {
            lastSearchValue = "";
        }
        String cateSearch = request.getParameter("cboCategory");
        if (cateSearch == null) {
            cateSearch = "";
        }
        String minPriceSearch = request.getParameter("txtMin");
        if (minPriceSearch == null) {
            minPriceSearch = "";
        }
        String maxPriceSearch = request.getParameter("txtMax");
        if (maxPriceSearch == null) {
            maxPriceSearch = "";
        }
        String errorContent = "There having error to add item to cart! Please try it again later";

        boolean error = false;

        try {
            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();

            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");

            if (dtoAccount == null) {
                url = LOGIN_PAGE;
                String itemId = request.getParameter("txtItemID");
                if (itemId != null) {
                    String itemBefore = (String) session.getAttribute("FOODTOADD");
                    if (itemBefore != null) {
                        session.setAttribute("FOODTOADD", null);
                    }
                    FoodOrDrinkDTO dtoFoodOrDrink = daoFoodOrDrink.searchFoodByIDToAdd(itemId);
                    if (dtoFoodOrDrink != null) {
                        dtoFoodOrDrink.setQuantity(1);
                        session.setAttribute("FOODTOADD", dtoFoodOrDrink);
                    } else {
                        error = true;
                        errorContent = "Sorry the item you want to add to cart no more exist! Please choose the other!";
                    }
                }
                if (error) {
                    request.setAttribute("ERRORTOADDTOCART", errorContent);
                }
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            error = true;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                String customerID = dtoAccount.getUserID();
                                if (customerID != null) {
                                    Cart shoppingCart = (Cart) session.getAttribute("shoppingCart");
                                    if (shoppingCart == null) {
                                        shoppingCart = new Cart(customerID);
                                    }
                                    String itemId = request.getParameter("txtItemID");
                                    if (itemId != null) {
                                        FoodOrDrinkDTO dtoFoodOrDrink = daoFoodOrDrink.searchFoodByIDToAdd(itemId);
                                        if (dtoFoodOrDrink != null) {
                                            dtoFoodOrDrink.setQuantity(1);
                                            shoppingCart.addToCart(dtoFoodOrDrink);
                                            session.setAttribute("shoppingCart", shoppingCart);
                                            url = "SearchForCustomer?txtSearchItem=" + lastSearchValue + "&page=" + page
                                                    + "&txtMin=" + minPriceSearch + "&txtMax=" + maxPriceSearch
                                                    + "&cboCategory=" + cateSearch + "&btAction=Search";
                                        } else {
                                            error = true;
                                            errorContent = "Sorry the item you want to add to cart no more exist! Please choose the other!";
                                        }
                                    }
                                    if (error) {
                                        request.setAttribute("ERRORTOADDTOCART", errorContent);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("AddToCartServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("AddToCartServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("AddToCartServlet_NullPointer " + ex.getMessage());
        } finally {
            if (error) {
                RequestDispatcher rd = request.getRequestDispatcher(url);
                rd.forward(request, response);
                out.close();
            } else {
                response.sendRedirect(url);
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
