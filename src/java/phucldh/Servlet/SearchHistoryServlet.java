/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.OrderDetail.OrderDetailDAO;
import phucldh.OrderDetail.OrderDetailDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SearchHistoryServlet", urlPatterns = {"/SearchHistoryServlet"})
public class SearchHistoryServlet extends HttpServlet {

    private final String HISTORY_PAGE = "showHistoryPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = HISTORY_PAGE;
        String inputDate = request.getParameter("txtShoppingDate");
        String txtSearchName = request.getParameter("txtSearchName");
        String errorContent = "Having problem to search history";

        try {
            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            UserOrderDAO daoUserOrder = new UserOrderDAO();
            OrderDetailDAO daoOrderDetail = new OrderDetailDAO();
            CouponsDAO daoCoupons = new CouponsDAO();

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                List<UserOrderDTO> listUserOrder = null;
                                if (!txtSearchName.equalsIgnoreCase("") && inputDate.equalsIgnoreCase("")) {
                                    if (listUserOrder != null) {
                                        listUserOrder.clear();
                                    }
                                    daoUserOrder.getSearchByName(txtSearchName, dtoAccount.getUserID());
                                    listUserOrder = daoUserOrder.ListSearch();
                                } else if (!inputDate.equalsIgnoreCase("") && txtSearchName.equalsIgnoreCase("")) {
                                    if (listUserOrder != null) {
                                        listUserOrder.clear();
                                    }
                                    daoUserOrder.getSearchByDate(inputDate, dtoAccount.getUserID());
                                    listUserOrder = daoUserOrder.ListSearch();
                                } else if (!inputDate.equalsIgnoreCase("") && !txtSearchName.equalsIgnoreCase("")) {
                                    if (listUserOrder != null) {
                                        listUserOrder.clear();
                                    }
                                    daoUserOrder.getSearchByNameOrDate(inputDate, txtSearchName, dtoAccount.getUserID());
                                    listUserOrder = daoUserOrder.ListSearch();
                                }
                                Map<UserOrderDTO, List<OrderDetailDTO>> map = new HashMap<>();
                                if (listUserOrder != null) {
                                    List<String> listOrderIDOfUserBefore = new ArrayList<>();
                                    List<String> listOrderIDOfUserAfter = new ArrayList<>();
                                    List<String> listOrderIDOfUserFilter = new ArrayList<>();
                                    for (UserOrderDTO userOrderDTO : listUserOrder) {
                                        listOrderIDOfUserBefore.add(userOrderDTO.getOrderID());
                                    }
                                    for (String element : listOrderIDOfUserBefore) {
                                        if (!listOrderIDOfUserAfter.contains(element)) {
                                            listOrderIDOfUserAfter.add(element);
                                        }
                                    }
                                    if (!txtSearchName.equalsIgnoreCase("")) {
                                        for (String id : listOrderIDOfUserAfter) {
                                            int count = 0;
                                            daoOrderDetail.getListProperty(id);
                                            List<String> list = daoOrderDetail.ListProperty();
                                            for (String property : list) {
                                                if (property.equalsIgnoreCase(txtSearchName)) {
                                                    count++;
                                                }
                                            }
                                            list.clear();
                                            if (count > 0) {
                                                listOrderIDOfUserFilter.add(id);
                                            }
                                        }
                                    } else {
                                        for (String string : listOrderIDOfUserAfter) {
                                            listOrderIDOfUserFilter.add(string);
                                        }
                                    }
                                    for (String string : listOrderIDOfUserFilter) {
                                        daoOrderDetail.getItem(string);
                                        List<OrderDetailDTO> list = daoOrderDetail.ListItemInOrder();
                                        UserOrderDTO dtoUserOrder = daoUserOrder.getOrder(string);
                                        map.put(dtoUserOrder, list);
                                    }

                                    request.setAttribute("HISTORYUSERORDER", map);
                                    daoCoupons.getListCoupons();
                                    List<CouponsDTO> listCoupons = daoCoupons.getTheList();
                                    session.setAttribute("LISTCOUPONSTOCHECK", listCoupons);
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("SearchHistoryServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("SearchHistoryServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("SearchHistoryServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
