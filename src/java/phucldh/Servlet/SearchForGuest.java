/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SearchForGuest", urlPatterns = {"/SearchForGuest"})
public class SearchForGuest extends HttpServlet {

    private final int RECORD_PER_PAGE = 20;
    private final String DEFAULT_PAGE = "homePage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int page = 1;
        int noOfRecord = 0;
        int amount = 0;
        
        String url = DEFAULT_PAGE;
        String searchValue = request.getParameter("txtSearchItem");
        String category = request.getParameter("cboCategory");
        String priceMin = request.getParameter("txtMin");
        String priceMax = request.getParameter("txtMax");
        String pageParam = request.getParameter("page");


        try {
            ServletContext context = getServletContext();
            
            HttpSession session = request.getSession(true);
            
            FoodOrDrinkDAO daoItem = new FoodOrDrinkDAO();
            
            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }

            if (searchValue.trim().isEmpty() && category.trim().equalsIgnoreCase("") && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                context.setAttribute("ITEMLIST", null);
                daoItem.getAllItem((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> listItem = daoItem.getListAllFoodOrDrink();
                context.setAttribute("ITEMLIST", listItem);
                noOfRecord = daoItem.countFoodForCustomer();
            }
            if (searchValue.trim().equalsIgnoreCase("") && category.trim().equalsIgnoreCase("NULL") && priceMin.trim().equalsIgnoreCase("") && priceMax.trim().equalsIgnoreCase("")) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.getAllItem((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> listItem = daoItem.getListAllFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", listItem);
                noOfRecord = daoItem.countFoodForCustomer();
            }
            if (!searchValue.trim().isEmpty() && category.trim().equalsIgnoreCase("NULL") && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByName(searchValue, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByName(searchValue);
            }
            if (!category.trim().equalsIgnoreCase("NULL") && !category.trim().equalsIgnoreCase("") && searchValue.trim().isEmpty() && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByCategory(category, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByCategory(category);
            }
            if (!priceMin.trim().isEmpty() && !priceMax.trim().isEmpty() && category.trim().equalsIgnoreCase("NULL") && searchValue.trim().isEmpty()) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByLimitPrice(priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByLimitPrice(priceMin, priceMax);
            }
            if (!searchValue.trim().isEmpty() && !category.trim().equalsIgnoreCase("NULL") && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByNameAndCategory(searchValue, category, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByNameAndCategory(searchValue, category);
            }
            if (!searchValue.trim().isEmpty() && !priceMin.trim().isEmpty() && !priceMax.trim().isEmpty() && category.trim().equalsIgnoreCase("NULL")) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByNameAndPrice(searchValue, priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByNameAndPrice(searchValue, priceMin, priceMax);
            }
            if (!priceMin.trim().isEmpty() && !priceMax.trim().isEmpty() && !category.trim().equalsIgnoreCase("NULL") && searchValue.trim().isEmpty()) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByCategoryAndPrice(category, priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByCategoryAndPrice(category, priceMin, priceMax);
            }
            if (!searchValue.trim().isEmpty() && !category.trim().equalsIgnoreCase("NULL") && !priceMin.trim().isEmpty() && !priceMax.trim().isEmpty()) {
                request.setAttribute("SEARCHITEMRESULT", null);
                daoItem.searchByAllFilter(searchValue, category, priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                request.setAttribute("SEARCHITEMRESULT", list);
                noOfRecord = daoItem.countRecordSearchByAllFilter(searchValue, category, priceMin, priceMax);
            }
            if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                session.setAttribute("NOOFPAGE", noOfPage);
                session.setAttribute("CURRENTPAGE", page);
            }
        } catch (SQLException ex) {
            log("SearchForGuest_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("SearchForGuest_Naming " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("SearchForGuest_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("SearchForGuest_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
