/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ApplyCouponsServlet", urlPatterns = {"/ApplyCouponsServlet"})
public class ApplyCouponsServlet extends HttpServlet {

    private final String VIEW_CART_PAGE = "viewCartPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = VIEW_CART_PAGE;
        String content = "There having error when apply the code coupons please try it again";

        boolean error = false;

        try {
            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            CouponsDAO daoCoupons = new CouponsDAO();

            session.setAttribute("COUPONSDTOINPUT", null);

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");

            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                content = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", content);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                String coupondCode = (String) session.getAttribute("VALUETRANSFERCOUPONSCODE");
                                if (coupondCode != null) {
                                    CouponsDTO dtoCoupons = daoCoupons.getCouponsByCode(coupondCode);
                                    if (dtoCoupons != null) {
                                        List<String> listIDItem = (List) session.getAttribute("LISTID");
                                        if (listIDItem != null) {
                                            boolean checkCoupon = false;
                                            for (String string : listIDItem) {
                                                if (string.equalsIgnoreCase(dtoCoupons.getDescription())) {
                                                    session.setAttribute("COUPONSDTOINPUT", dtoCoupons);
                                                    checkCoupon = true;
                                                }
                                            }
                                            if (!checkCoupon) {
                                                error = true;
                                                content = "Coupons can't apply.Because item not in discount coupons";
                                            }
                                        }
                                    } else {
                                        error = true;
                                        content = "Coupons don't exist please check it again";
                                    }
                                    if (error) {
                                        request.setAttribute("ERRORAPPLYCOUPONS", content);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("ApplyCouponsServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("ApplyCouponsServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("ApplyCouponsServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
