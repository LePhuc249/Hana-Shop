/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Cart.Cart;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.Feedback.FeedbackDAO;
import phucldh.Feedback.FeedbackDTO;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.OrderDetail.OrderDetailDAO;
import phucldh.OrderDetail.OrderDetailDTO;
import phucldh.Relationship_Category.Relationship_CategoryDAO;
import phucldh.Relationship_Category.Relationship_CategoryDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "TransferServlet", urlPatterns = {"/TransferServlet"})
public class TransferServlet extends HttpServlet {

    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String HOME_PAGE = "homePage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String UPDATE_PAGE = "updateItemPage";
    private final String DETAIL_ITEM_PAGE = "viewDetailItemPage";
    private final String FEEDBACK_PAGE = "feedbackPage";
    private final String REMOVE_ITEM_SERVLET = "DeleteCartServlet";
    private final String UPDATE_QUANTITY_ITEM_SERVLET = "UpdateCartServlet";
    private final String APPLY_CODE_SERVLET = "ApplyCouponsServlet";
    private final String CHECK_OUT_SERVLET = "OrderServlet";
    private final String DELETE_ORDER_SERVLET = "DeleteOrderServlet";
    private final String HISTORY_PAGE = "showHistoryPage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String action = request.getParameter("btAction");
        String url = "";
        boolean error = false;
        String errorContent = "You can't send feedback if you haven't finish the order";

        try {
            HttpSession session = request.getSession(false);
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = HOME_PAGE;
                if (action != null) {
                    if (action.equalsIgnoreCase("ShowDetail")) {
                        String itemID = request.getParameter("id");
                        if (itemID != null) {
                            int page = 1;
                            String pageParam = request.getParameter("page");
                            String lastSearchValue = (String) request.getParameter("txtSearchItem");
                            if (lastSearchValue == null) {
                                lastSearchValue = "";
                            }
                            String cateSearch = request.getParameter("cboCategory");
                            if (cateSearch == null) {
                                cateSearch = "";
                            }
                            String minPriceSearch = request.getParameter("txtMin");
                            if (minPriceSearch == null) {
                                minPriceSearch = "";
                            }
                            String maxPriceSearch = request.getParameter("txtMax");
                            if (maxPriceSearch == null) {
                                maxPriceSearch = "";
                            }
                            if (pageParam != null) {
                                page = Integer.parseInt(pageParam);
                            }
                            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();
                            FoodOrDrinkDTO dtoFoood = daoFoodOrDrink.searchFoodByID(itemID);
                            if (dtoFoood != null) {
                                String category = dtoFoood.getCategoryID();
                                if (category != null) {
                                    Relationship_CategoryDAO daoRC = new Relationship_CategoryDAO();
                                    Relationship_CategoryDTO dtoRC = daoRC.getRelation(category);
                                    if (dtoRC != null) {
                                        if (category.equals(dtoRC.getCategoryID1())) {
                                            category = dtoRC.getCategoryID2();
                                        } else if (category.equals(dtoRC.getCategoryID2())) {
                                            category = dtoRC.getCategoryID1();
                                        }
                                        daoFoodOrDrink.getSuggestFoodByCategory(category);
                                        List<FoodOrDrinkDTO> listSuggest = daoFoodOrDrink.getSuggestList();
                                        if (listSuggest != null) {
                                            session.setAttribute("SUGGESTLISTBYCATE", listSuggest);
                                        }
                                        url = DETAIL_ITEM_PAGE;
                                        request.setAttribute("ITEMSHOW", dtoFoood);
                                        request.setAttribute("NAMESEARCH", lastSearchValue);
                                        request.setAttribute("CATEGORYSEARCH", cateSearch);
                                        request.setAttribute("MINSEARCH", minPriceSearch);
                                        request.setAttribute("MAXSEARCH", maxPriceSearch);
                                        request.setAttribute("PAGESEARCH", page);
                                    } else {
                                        url = DETAIL_ITEM_PAGE;
                                        request.setAttribute("ITEMSHOW", dtoFoood);
                                        request.setAttribute("NAMESEARCH", lastSearchValue);
                                        request.setAttribute("CATEGORYSEARCH", cateSearch);
                                        request.setAttribute("MINSEARCH", minPriceSearch);
                                        request.setAttribute("MAXSEARCH", maxPriceSearch);
                                        request.setAttribute("PAGESEARCH", page);
                                    }
                                }
                            }
                            CouponsDAO daoCoupons = new CouponsDAO();
                            daoCoupons.getCouponByItemID(itemID);
                            List<CouponsDTO> listCouponByID = daoCoupons.getTheListItem();
                            if (listCouponByID != null) {
                                request.setAttribute("LISTCOUPONSFORITEM", listCouponByID);
                            }
                        }
                    }
                }
            } else if (dtoAccount != null) {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        session.setAttribute("errorROLE", null);
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            if (action != null) {
                                if (action.equalsIgnoreCase("Update")) {
                                    String idUpdate = request.getParameter("id");
                                    if (idUpdate != null) {
                                        String lastSearchValueUpdate = request.getParameter("searchValue");
                                        if (lastSearchValueUpdate == null) {
                                            lastSearchValueUpdate = "";
                                        }
                                        String lastCategoryUpdate = request.getParameter("searchCate");
                                        if (lastCategoryUpdate == null) {
                                            lastCategoryUpdate = "";
                                        }
                                        String lastMinValueUpdate = request.getParameter("searchMin");
                                        if (lastMinValueUpdate == null) {
                                            lastMinValueUpdate = "";
                                        }
                                        String lastMaxValueUpdate = request.getParameter("searchMax");
                                        if (lastMaxValueUpdate == null) {
                                            lastMaxValueUpdate = "";
                                        }
                                        String pageParamUpdate = request.getParameter("page");
                                        FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();
                                        FoodOrDrinkDTO dtoFoodOrDrink = daoFoodOrDrink.getFoodOrDrinkByID(idUpdate);
                                        if (dtoFoodOrDrink != null) {
                                            session.setAttribute("UPDATEFOODDRINK", dtoFoodOrDrink);
                                            session.setAttribute("UPDATENAMESEARCH", lastSearchValueUpdate);
                                            session.setAttribute("UPDATECATEGORYSEARCH", lastCategoryUpdate);
                                            session.setAttribute("UPDATEMINSEARCH", lastMinValueUpdate);
                                            session.setAttribute("UPDATEMAXSEARCH", lastMaxValueUpdate);
                                            session.setAttribute("UPDATEPAGESEARCH", pageParamUpdate);
                                            url = UPDATE_PAGE;
                                        }
                                    }
                                }
                            }
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            url = CUSTOMER_PAGE;
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                if (action != null) {
                                    if (action.equalsIgnoreCase("ShowDetail")) {
                                        String itemID = request.getParameter("id");
                                        if (itemID != null) {
                                            int page = 1;
                                            String pageParam = request.getParameter("page");
                                            String lastSearchValue = (String) request.getParameter("txtSearchItem");
                                            if (lastSearchValue == null) {
                                                lastSearchValue = "";
                                            }
                                            String cateSearch = request.getParameter("cboCategory");
                                            if (cateSearch == null) {
                                                cateSearch = "";
                                            }
                                            String minPriceSearch = request.getParameter("txtMin");
                                            if (minPriceSearch == null) {
                                                minPriceSearch = "";
                                            }
                                            String maxPriceSearch = request.getParameter("txtMax");
                                            if (maxPriceSearch == null) {
                                                maxPriceSearch = "";
                                            }
                                            if (pageParam != null) {
                                                page = Integer.parseInt(pageParam);
                                            }
                                            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();
                                            FoodOrDrinkDTO dtoFoood = daoFoodOrDrink.searchFoodByID(itemID);
                                            if (dtoFoood != null) {
                                                String category = dtoFoood.getCategoryID();
                                                if (category != null) {
                                                    Relationship_CategoryDAO daoRC = new Relationship_CategoryDAO();
                                                    Relationship_CategoryDTO dtoRC = daoRC.getRelation(category);
                                                    if (dtoRC != null) {
                                                        if (category.equals(dtoRC.getCategoryID1())) {
                                                            category = dtoRC.getCategoryID2();
                                                        } else if (category.equals(dtoRC.getCategoryID2())) {
                                                            category = dtoRC.getCategoryID1();
                                                        }
                                                        daoFoodOrDrink.getSuggestFoodByCategory(category);
                                                        List<FoodOrDrinkDTO> listSuggest = daoFoodOrDrink.getSuggestList();
                                                        if (listSuggest != null) {
                                                            session.setAttribute("SUGGESTLISTBYCATE", listSuggest);
                                                        }
                                                        url = DETAIL_ITEM_PAGE;
                                                        request.setAttribute("ITEMSHOW", dtoFoood);
                                                        request.setAttribute("NAMESEARCH", lastSearchValue);
                                                        request.setAttribute("CATEGORYSEARCH", cateSearch);
                                                        request.setAttribute("MINSEARCH", minPriceSearch);
                                                        request.setAttribute("MAXSEARCH", maxPriceSearch);
                                                        request.setAttribute("PAGESEARCH", page);
                                                    } else {
                                                        url = DETAIL_ITEM_PAGE;
                                                        request.setAttribute("ITEMSHOW", dtoFoood);
                                                        request.setAttribute("NAMESEARCH", lastSearchValue);
                                                        request.setAttribute("CATEGORYSEARCH", cateSearch);
                                                        request.setAttribute("MINSEARCH", minPriceSearch);
                                                        request.setAttribute("MAXSEARCH", maxPriceSearch);
                                                        request.setAttribute("PAGESEARCH", page);
                                                    }
                                                }
                                            }
                                            CouponsDAO daoCoupons = new CouponsDAO();
                                            daoCoupons.getCouponByItemID(itemID);
                                            List<CouponsDTO> listCouponByID = daoCoupons.getTheListItem();
                                            if (listCouponByID != null) {
                                                request.setAttribute("LISTCOUPONSFORITEM", listCouponByID);
                                            }
                                        }
                                    } else if (action.equalsIgnoreCase("RemoveFromCart")) {
                                        String[] listID = request.getParameterValues("chkRemove");
                                        if (listID != null) {
                                            List<String> listDeleteID = new ArrayList<>();
                                            for (String id : listID) {
                                                listDeleteID.add(id);
                                            }
                                            request.setAttribute("LISTDELETED", listDeleteID);
                                            url = REMOVE_ITEM_SERVLET;
                                        }
                                    } else if (action.equalsIgnoreCase("UpdateFromCart")) {
                                        List<String> listUpdateID = new ArrayList<>();
                                        List<String> listUpdateQuantity = new ArrayList<>();
                                        String[] listID = request.getParameterValues("txtItemID");
                                        if (listID != null) {
                                            for (String id : listID) {
                                                listUpdateID.add(id);
                                            }
                                        }
                                        String[] listQuantity = request.getParameterValues("txtQuantity");
                                        if (listQuantity != null) {
                                            for (String id : listQuantity) {
                                                listUpdateQuantity.add(id);
                                            }
                                        }
                                        if (listUpdateID != null && listUpdateQuantity != null && listUpdateID.size() == listUpdateQuantity.size()) {
                                            request.setAttribute("LISTUDAPTEID", listUpdateID);
                                            request.setAttribute("LISTUDAPTEQUANTITY", listUpdateQuantity);
                                            url = UPDATE_QUANTITY_ITEM_SERVLET;
                                        }
                                    } else if (action.equalsIgnoreCase("Apply")) {
                                        List<String> listIDCheck = new ArrayList<>();
                                        String couponsCode = request.getParameter("txtCouponCode");
                                        Cart shoppingCart = (Cart) session.getAttribute("shoppingCart");
                                        if (shoppingCart != null) {
                                            HashMap<String, FoodOrDrinkDTO> listItemInCart = shoppingCart.getShoppingCart();
                                            for (FoodOrDrinkDTO value : listItemInCart.values()) {
                                                listIDCheck.add(value.getItemID());
                                            }
                                            url = APPLY_CODE_SERVLET;
                                            session.setAttribute("VALUETRANSFERCOUPONSCODE", couponsCode);
                                            session.setAttribute("LISTID", listIDCheck);
                                        }

                                    } else if (action.equalsIgnoreCase("Check Out")) {
                                        String totalNow = request.getParameter("txtTotalPrice");
                                        String paymentMehthod = request.getParameter("cboPaymentMethod");
                                        url = CHECK_OUT_SERVLET;
                                        session.setAttribute("VALUETRANSFERTOTALPRICE", totalNow);
                                        session.setAttribute("PAYMENTMETHODCHOOOSE", paymentMehthod);
                                    } else if (action.equalsIgnoreCase("Delete Order")) {
                                        String id = request.getParameter("txtOrderID");
                                        String nameSearch = request.getParameter("valueSearchName");
                                        String dateSearch = request.getParameter("valueSearchDate");
                                        request.setAttribute("ORDERIDDELETE", id);
                                        request.setAttribute("NAMESEARCHHISTORY", nameSearch);
                                        request.setAttribute("DATESEARCHHISTORY", dateSearch);
                                        url = DELETE_ORDER_SERVLET;
                                    } else if (action.equalsIgnoreCase("Feedback")) {
                                        String id = request.getParameter("txtOrderID");
                                        UserOrderDAO daoUserOrder = new UserOrderDAO();
                                        UserOrderDTO dtoUserOrder = daoUserOrder.getOrder(id);
                                        if (dtoUserOrder != null) {
                                            String statusOrder = dtoUserOrder.getStatus();
                                            if (statusOrder.equalsIgnoreCase("Fi")) {
                                                UserOrderDTO dtoOrder = daoUserOrder.getOrder(id);
                                                if (dtoOrder != null) {
                                                    FeedbackDAO daoFeedback = new FeedbackDAO();
                                                    FeedbackDTO dtoFeedback = daoFeedback.checkFeedbackExist(dtoAccount.getUserID(), dtoOrder.getOrderID());
                                                    if (dtoFeedback == null) {
                                                        OrderDetailDAO daoOrderDetail = new OrderDetailDAO();
                                                        daoOrderDetail.getItem(dtoOrder.getOrderID());
                                                        List<OrderDetailDTO> listDetail = daoOrderDetail.ListItemInOrder();
                                                        if (listDetail != null) {
                                                            CouponsDAO daoCoupon = new CouponsDAO();
                                                            daoCoupon.getListCoupons();
                                                            List<CouponsDTO> listCoupons = daoCoupon.getTheList();
                                                            if (listCoupons != null) {
                                                                session.setAttribute("ORDER", dtoOrder);
                                                                session.setAttribute("ORDERDETAILLIST", listDetail);
                                                                session.setAttribute("COUPONSLIST", listCoupons);
                                                                url = FEEDBACK_PAGE;
                                                            }
                                                        }
                                                    } else {
                                                        OrderDetailDAO daoOrderDetail = new OrderDetailDAO();
                                                        daoOrderDetail.getItem(dtoOrder.getOrderID());
                                                        List<OrderDetailDTO> listDetail = daoOrderDetail.ListItemInOrder();
                                                        if (listDetail != null) {
                                                            CouponsDAO daoCoupon = new CouponsDAO();
                                                            daoCoupon.getListCoupons();
                                                            List<CouponsDTO> listCoupons = daoCoupon.getTheList();
                                                            if (listCoupons != null) {
                                                                session.setAttribute("ORDER", dtoOrder);
                                                                session.setAttribute("ORDERDETAILLIST", listDetail);
                                                                session.setAttribute("COUPONSLIST", listCoupons);
                                                                session.setAttribute("FEEDBACKEXIST", dtoFeedback);
                                                                url = FEEDBACK_PAGE;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else if (statusOrder.equalsIgnoreCase("Ca")) {
                                                error = true;
                                                url = HISTORY_PAGE;
                                                errorContent = "You can't feedback the order you have been cancel";
                                            } else {
                                                error = true;
                                                url = HISTORY_PAGE;
                                                errorContent = "You can't feedback if the order haven't finish";
                                            }
                                            if (error) {
                                                request.setAttribute("STATUSFEEDBACKERROR", errorContent);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            log("TransferServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("TransferServlet_Naming " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("TransferServlet_NullPointer " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
