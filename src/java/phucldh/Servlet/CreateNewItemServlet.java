/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.FoodOrDrink.FoodOrDrinkCreateError;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CreateNewItemServlet", urlPatterns = {"/CreateNewItemServlet"})
public class CreateNewItemServlet extends HttpServlet {

    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final int RECORD_PER_PAGE = 20;
    private final String DEFAULT_STATUS = "Active";
    private final String DEFAULT_CREATE_ITEM_PAGE = "createItemPage";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        float price = 0;

        int quantity = 0;
        int page = 1;
        int noOfRecord = 0;

        String url = DEFAULT_CREATE_ITEM_PAGE;

        boolean error = false;

        FoodOrDrinkCreateError errors = new FoodOrDrinkCreateError();

        try {
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            FoodOrDrinkDAO daoItem = new FoodOrDrinkDAO();

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                            if (isMultipart) {
                                FileItemFactory factory = new DiskFileItemFactory();
                                ServletFileUpload upload = new ServletFileUpload(factory);
                                List<FileItem> items = upload.parseRequest(request);
                                Iterator iter = items.iterator();
                                Hashtable param = new Hashtable();
                                String filename = null;
                                String itemImage = null;
                                while (iter.hasNext()) {
                                    FileItem item = (FileItem) iter.next();
                                    if (item.isFormField()) {
                                        param.put(item.getFieldName(), item.getString());
                                    } else {
                                        try {
                                            String itemName = item.getName();
                                            if (!itemName.isEmpty()) {
                                                filename = itemName.substring(itemName.lastIndexOf("//") + 1);
                                                itemImage = "img/" + filename;
                                                String realPath = getServletContext().getRealPath("/") + "img\\" + filename;
                                                File saveFile = new File(realPath);
                                                item.write(saveFile);
                                            } else {
                                                error = true;
                                                errors.setImageError("Image can't empty");
                                            }
                                        } catch (IOException ex) {
                                            log("CreateItemServlet_IO " + ex.getMessage());
                                        }
                                    }
                                }
                                String itemID = null;
                                String itemNa = (String) param.get("txtItemname");
                                if (itemNa.trim().length() < 6 || itemNa.trim().length() > 50) {
                                    error = true;
                                    errors.setNameLengthError("Name must have length from 6-50 character");
                                }
                                String itemDescription = (String) param.get("txtItemdescription");
                                if (itemDescription.trim().length() < 6 || itemDescription.trim().length() > 100) {
                                    error = true;
                                    errors.setDescriptionLengthError("Description must have length from 6-100 character");
                                }
                                String priceString = (String) param.get("txtItemprice");
                                if (priceString.length() < 1 || priceString.length() > 5) {
                                    error = true;
                                    errors.setPriceError("Price must from 0  to 9999");
                                }
                                String productName = (String) param.get("txtItemProductName");
                                if (productName.trim().length() < 6 || productName.trim().length() > 50) {
                                    error = true;
                                    errors.setProductNameLengthError("Name product must have length from 6-50 character");
                                }
                                String quantityString = (String) param.get("txtItemquantity");
                                if (quantityString.trim().length() < 1 || quantityString.trim().length() > 5) {
                                    error = true;
                                    errors.setQuantityError("Quantity must from 0  to 9999");
                                }
                                String status = DEFAULT_STATUS;
                                String cateID = (String) param.get("cboCategory");
                                if (cateID == null || cateID.isEmpty()) {
                                    error = true;
                                    errors.setCategoryError("Category must have length from 6-50 character");
                                }
                                boolean checkExist = daoItem.checkExistFood(itemImage, itemNa, itemDescription, productName);
                                if (checkExist) {
                                    error = true;
                                    errors.setNameLengthError("Have been exist item with this property");
                                    errors.setImageError("Have been exist item with this property");
                                    errors.setDescriptionLengthError("Have been exist item with this property");
                                    errors.setProductNameLengthError("Have been exist item with this property");
                                }
                                if (error) {
                                    request.setAttribute("ERRORCREATEITEM", errors);
                                } else {
                                    String lastItemID = daoItem.getLastItemID();
                                    if (lastItemID == null) {
                                        itemID = "FD_1";
                                    } else {
                                        int count = Integer.parseInt(lastItemID.split("_")[1].trim());
                                        itemID = "FD" + "_" + (count + 1);
                                    }
                                    if (priceString.length() < 5) {
                                        price = Float.parseFloat(priceString);
                                    }
                                    if (quantityString.length() < 5) {
                                        quantity = Integer.parseInt(quantityString);
                                    }
                                    FoodOrDrinkDTO dtoFood = new FoodOrDrinkDTO(itemID, itemNa, itemImage, itemDescription, price, productName, quantity, status, cateID);
                                    boolean result = daoItem.insertItem(dtoFood);
                                    if (result) {
                                        url = "SearchForAdmin?txtSearchItem=&txtMin=&txtMax=&cboCategory=&btAction=SearchForAdmin";

                                        String pageParam = request.getParameter("page");
                                        if (pageParam != null) {
                                            page = Integer.parseInt(pageParam);
                                        }

                                        daoItem.getAllItemForAdmin((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                        List<FoodOrDrinkDTO> listItem = daoItem.getListAllFoodOrDrink();

                                        context.setAttribute("ITEMLIST", null);
                                        context.setAttribute("ITEMLIST", listItem);

                                        noOfRecord = daoItem.countFoodForAdmin();
                                        int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);

                                        session.setAttribute("NOOFPAGE", noOfPage);
                                        session.setAttribute("CURRENTPAGE", page);
                                    }
                                }
                            }
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            url = CUSTOMER_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            log("CreateItemServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("CreateItemServlet_Naming " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("CreateItemServlet_NumberFormat " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("CreateItemServlet_NullPointer " + ex.getMessage());
        } catch (FileUploadException ex) {
            log("CreateItemServlet_FileUpload " + ex.getMessage());
        } catch (Exception ex) {
            log("CreateItemServlet_Exception " + ex.getMessage());
        } finally {
            if (error) {
                RequestDispatcher rd = request.getRequestDispatcher(url);
                rd.forward(request, response);
            } else {
                response.sendRedirect(url);
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
