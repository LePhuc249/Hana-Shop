/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "SearchForCustomer", urlPatterns = {"/SearchForCustomer"})
public class SearchForCustomer extends HttpServlet {

    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final int RECORD_PER_PAGE = 20;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int page = 1;
        int noOfRecord = 0;
        int amount = 0;

        String url = CUSTOMER_PAGE;
        String searchValue = request.getParameter("txtSearchItem");
        String category = request.getParameter("cboCategory");
        String priceMin = request.getParameter("txtMin");
        String priceMax = request.getParameter("txtMax");
        String pageParam = request.getParameter("page");
        String errorContent = "Having a problem when search";

        try {
            ServletContext context = getServletContext();

            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            FoodOrDrinkDAO daoItem = new FoodOrDrinkDAO();

            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                if (searchValue.trim().isEmpty() && category.trim().equalsIgnoreCase("") && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                                    context.setAttribute("ITEMLIST", null);
                                    daoItem.getAllItem((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> listItem = daoItem.getListAllFoodOrDrink();
                                    context.setAttribute("ITEMLIST", listItem);
                                    noOfRecord = daoItem.countFoodForCustomer();
                                }
                                if (searchValue.trim().equalsIgnoreCase("") && category.trim().equalsIgnoreCase("NULL") && priceMin.trim().equalsIgnoreCase("") && priceMax.trim().equalsIgnoreCase("")) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.getAllItem((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> listItem = daoItem.getListAllFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", listItem);
                                    noOfRecord = daoItem.countFoodForCustomer();
                                }
                                if (!searchValue.trim().isEmpty() && category.trim().equalsIgnoreCase("NULL") && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByName(searchValue, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByName(searchValue);
                                }
                                if (!category.trim().equalsIgnoreCase("NULL") && !category.trim().equalsIgnoreCase("") && searchValue.trim().isEmpty() && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByCategory(category, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByCategory(category);
                                }
                                if (!priceMin.trim().isEmpty() && !priceMax.trim().isEmpty() && category.trim().equalsIgnoreCase("NULL") && searchValue.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByLimitPrice(priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByLimitPrice(priceMin, priceMax);
                                }
                                if (!searchValue.trim().isEmpty() && !category.trim().equalsIgnoreCase("NULL") && priceMin.trim().isEmpty() && priceMax.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByNameAndCategory(searchValue, category, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByNameAndCategory(searchValue, category);
                                }
                                if (!searchValue.trim().isEmpty() && !priceMin.trim().isEmpty() && !priceMax.trim().isEmpty() && category.trim().equalsIgnoreCase("NULL")) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByNameAndPrice(searchValue, priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByNameAndPrice(searchValue, priceMin, priceMax);
                                }
                                if (!priceMin.trim().isEmpty() && !priceMax.trim().isEmpty() && !category.trim().equalsIgnoreCase("NULL") && searchValue.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByCategoryAndPrice(category, priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByCategoryAndPrice(category, priceMin, priceMax);
                                }
                                if (!searchValue.trim().isEmpty() && !category.trim().equalsIgnoreCase("NULL") && !priceMin.trim().isEmpty() && !priceMax.trim().isEmpty()) {
                                    request.setAttribute("SEARCHITEMRESULT", null);
                                    daoItem.searchByAllFilter(searchValue, category, priceMin, priceMax, (page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                    List<FoodOrDrinkDTO> list = daoItem.getListFoodOrDrink();
                                    request.setAttribute("SEARCHITEMRESULT", list);
                                    noOfRecord = daoItem.countRecordSearchByAllFilter(searchValue, category, priceMin, priceMax);
                                }
                                if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                                    int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                                    session.setAttribute("NOOFPAGE", noOfPage);
                                    session.setAttribute("CURRENTPAGE", page);
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("SearchForCustomer_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("SearchForCustomer_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("SearchForCustomer_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("SearchForCustomer_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
