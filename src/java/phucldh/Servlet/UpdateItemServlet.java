/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.FoodOrDrink.FoodOrDrinkCreateError;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.Role.RoleDAO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "UpdateItemServlet", urlPatterns = {"/UpdateItemServlet"})
public class UpdateItemServlet extends HttpServlet {

    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String UPDATE_PAGE = "updateItemPage";
    private final int RECORD_PER_PAGE = 20;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        float price = 0;

        int quantity = 0;
        int counter = 0;
        int numError = 0;
        int page = 1;
        int noOfRecord = 0;

        String urlRewriting = UPDATE_PAGE;

        boolean error = false;
        boolean errorAtPrice = false;
        boolean errorAtQuantity = false;
        boolean errorAtCount = false;

        FoodOrDrinkCreateError errors = new FoodOrDrinkCreateError();

        try {
            ServletContext context = getServletContext();
            HttpSession session = request.getSession(false);
            FoodOrDrinkDAO daoItem = new FoodOrDrinkDAO();
            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                urlRewriting = LOGIN_PAGE;
            } else {
                AccountDAO daoAccount = new AccountDAO();
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    RoleDAO daoRole = new RoleDAO();
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            String pageParam = (String) session.getAttribute("UPDATEPAGESEARCH");
                            String lastSearchValue = (String) session.getAttribute("UPDATENAMESEARCH");
                            if (lastSearchValue == null) {
                                lastSearchValue = "";
                            }
                            String cateSearch = (String) session.getAttribute("UPDATECATEGORYSEARCH");
                            if (cateSearch == null) {
                                cateSearch = "";
                            }
                            String minPriceSearch = (String) session.getAttribute("UPDATEMINSEARCH");
                            if (minPriceSearch == null) {
                                minPriceSearch = "";
                            }
                            String maxPriceSearch = (String) session.getAttribute("UPDATEMAXSEARCH");
                            if (maxPriceSearch == null) {
                                maxPriceSearch = "";
                            }
                            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                            if (isMultipart) {
                                FileItemFactory factory = new DiskFileItemFactory();
                                ServletFileUpload upload = new ServletFileUpload(factory);
                                List<FileItem> items = upload.parseRequest(request);
                                Iterator iter = items.iterator();
                                Hashtable param = new Hashtable();
                                String filename = null;
                                String itemImage = null;
                                String itemName = null;
                                while (iter.hasNext()) {
                                    FileItem item = (FileItem) iter.next();
                                    if (item.isFormField()) {
                                        param.put(item.getFieldName(), item.getString());
                                    } else {
                                        try {
                                            itemName = item.getName();
                                            if (!itemName.isEmpty()) {
                                                filename = itemName.substring(itemName.lastIndexOf("//") + 1);
                                                itemImage = "img/" + filename;
                                                String realPath = getServletContext().getRealPath("/") + "img\\" + filename;
                                                File saveFile = new File(realPath);
                                                item.write(saveFile);
                                            }
                                        } catch (IOException ex) {
                                            log("UpdateItemServlet_IO " + ex.getMessage());
                                        }
                                    }
                                }
                                String itemID = (String) param.get("txtItemID");
                                FoodOrDrinkDTO dtoFoodOrDrinkOld = daoItem.getFoodOrDrinkByID(itemID);
                                String itemNa = (String) param.get("txtItemname");
                                if (itemNa.trim().length() < 6 || itemNa.trim().length() > 50) {
                                    error = true;
                                    errors.setNameLengthError("Name must have length from 6-50 character");
                                }
                                if (itemName.isEmpty()) {
                                    itemImage = (String) param.get("imgURL");
                                }
                                String itemDescription = (String) param.get("txtItemdescription");
                                if (itemDescription.trim().length() < 6 || itemDescription.trim().length() > 100) {
                                    error = true;
                                    errors.setDescriptionLengthError("Description must have length from 6-100 character");
                                }
                                String priceString = (String) param.get("txtItemprice");
                                if (priceString.length() < 1 || priceString.length() > 5) {
                                    error = true;
                                    errors.setPriceError("Price must from 0  to 9999");
                                    errorAtPrice = true;
                                }
                                String productName = (String) param.get("txtItemProductName");
                                if (productName.trim().length() < 6 || productName.trim().length() > 50) {
                                    error = true;
                                    errors.setProductNameLengthError("Name product must have length from 6-50 character");
                                }
                                String quantityString = (String) param.get("txtItemquantity");
                                if (quantityString.trim().length() < 1 || quantityString.trim().length() > 5) {
                                    error = true;
                                    errors.setQuantityError("Quantity must from 0 to 9999");
                                    errorAtQuantity = true;
                                }
                                String status = (String) param.get("cboStatus");
                                String cateID = (String) param.get("cboCategory");
                                if (cateID.isEmpty()) {
                                    error = true;
                                    errors.setCategoryError("Category must have length from 6-50 character");
                                }
                                String counterString = (String) param.get("txtCounter");
                                if (counterString.length() < 1 || counterString.length() > 3) {
                                    error = true;
                                    errors.setCounterError("The counter must have length from 1-999 character");
                                    errorAtCount = true;
                                }
                                if (!errorAtPrice) {
                                    if (priceString.length() < 5) {
                                        price = Float.parseFloat(priceString);
                                        if (price <= 0) {
                                            error = true;
                                            errors.setPriceError("Price can't equal 0");
                                        }
                                    }
                                }
                                if (!errorAtQuantity) {
                                    if (quantityString.length() < 5 && quantityString != null) {
                                        quantity = Integer.parseInt(quantityString);
                                    }
                                }
                                if (!errorAtCount) {
                                    if (counterString.length() < 3 && quantityString != null) {
                                        counter = Integer.parseInt(counterString);
                                    }
                                }
                                if (dtoFoodOrDrinkOld != null) {
                                    if (itemNa.equalsIgnoreCase(dtoFoodOrDrinkOld.getItemName())) {
                                        numError++;
                                    }
                                    if (itemDescription.equalsIgnoreCase(dtoFoodOrDrinkOld.getDescription())) {
                                        numError++;
                                    }
                                    if (productName.equalsIgnoreCase(dtoFoodOrDrinkOld.getProductName())) {
                                        numError++;
                                    }
                                    if (cateID != null) {
                                        if (cateID.equalsIgnoreCase(dtoFoodOrDrinkOld.getCategoryID())) {
                                            numError++;
                                        }
                                    }
                                    if (price == dtoFoodOrDrinkOld.getPrice()) {
                                        numError++;
                                    }
                                    if (quantity == dtoFoodOrDrinkOld.getQuantity()) {
                                        numError++;
                                    }
                                    if (counter == dtoFoodOrDrinkOld.getCounter()) {
                                        numError++;
                                    }
                                    if (status.equalsIgnoreCase(dtoFoodOrDrinkOld.getStatus())) {
                                        numError++;
                                    }
                                }
                                if (numError == 8) {
                                    error = true;
                                    errors.setNameLengthError(null);
                                    errors.setDescriptionLengthError(null);
                                    errors.setProductNameLengthError(null);
                                    errors.setCategoryError(null);
                                    errors.setPriceError(null);
                                    errors.setQuantityError(null);
                                    errors.setCounterError(null);
                                    errors.setNoThingChange("No thing have been change please change some thing!!!");
                                } else if (numError < 8) {
                                    if (errors.getCategoryError() != null || errors.getCounterError() != null || errors.getDescriptionLengthError() != null || errors.getNameLengthError() != null || errors.getProductNameLengthError() != null || errors.getPriceError() != null || errors.getQuantityError() != null || errors.getImageError() != null) {
                                        error = true;
                                    } else {
                                        error = false;
                                    }
                                }
                                boolean checkExist = daoItem.checkExistFood(itemImage, itemNa, itemDescription, productName);
                                if (checkExist) {
                                    error = true;
                                    errors.setNameLengthError("Have been exist item with this property");
                                    errors.setImageError("Have been exist item with this property");
                                    errors.setDescriptionLengthError("Have been exist item with this property");
                                    errors.setProductNameLengthError("Have been exist item with this property");
                                }
                                if (error) {
                                    request.setAttribute("ERRORUPDATEITEM", errors);
                                } else {
                                    if (pageParam != null) {
                                        page = Integer.parseInt(pageParam);
                                    }
                                    FoodOrDrinkDTO dtoFood = new FoodOrDrinkDTO(itemID, itemNa, itemImage, itemDescription, price, productName, quantity, status, cateID, counter);
                                    boolean result = daoItem.updateItem(dtoFood);
                                    if (result) {
                                        session.setAttribute("UPDATEFOODDRINK", null);
                                        session.setAttribute("UPDATENAMESEARCH", null);
                                        session.setAttribute("UPDATECATEGORYSEARCH", null);
                                        session.setAttribute("UPDATEMINSEARCH", null);
                                        session.setAttribute("UPDATEMAXSEARCH", null);
                                        session.setAttribute("UPDATEPAGESEARCH", null);
                                        urlRewriting = "SearchForAdmin?txtSearchItem=" + lastSearchValue + "&page=" + page
                                                + "&txtMin=" + minPriceSearch + "&txtMax=" + maxPriceSearch
                                                + "&cboCategory=" + cateSearch + "&btAction=SearchForAdmin";
                                    }
                                }
                            }
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            urlRewriting = CUSTOMER_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            log("UpdateItemServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("UpdateItemServlet_Naming " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("UpdateItemServlet_NumberFormat " + ex.getMessage());
        } catch (FileUploadException ex) {
            log("UpdateItemServlet_FileUpload " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("UpdateItemServlet_NullPointer " + ex.getMessage());
        } catch (Exception ex) {
            log("UpdateItemServlet_Exception " + ex.getMessage());
        } finally {
            if (error) {
                RequestDispatcher rd = request.getRequestDispatcher(urlRewriting);
                rd.forward(request, response);
                out.close();
            } else {
                response.sendRedirect(urlRewriting);
                out.close();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
