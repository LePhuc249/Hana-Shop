/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Category.CategoryDAO;
import phucldh.Category.CategoryDTO;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.PaymentMethod.PaymentMethodDAO;
import phucldh.PaymentMethod.PaymentMethodDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "StartUpServlet", urlPatterns = {"/StartUpServlet"})
public class StartUpServlet extends HttpServlet {

    private final int RECORD_PER_PAGE = 20;
    private final String DEFAULT_PAGE = "homePage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String VERIFY_PAGE = "verifyCodePage";
    private final String DATE_FORMAT = "yyyy-MM-dd";
    private final String SECURE_HASH_ALGORITHM = "SHA-256";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int page = 1;
        int noOfRecord;

        String url = DEFAULT_PAGE;
        String pageParam = request.getParameter("page");
        String dateNow = (java.time.LocalDate.now().toString()).trim();

        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);

        try {
            ServletContext context = getServletContext();

            HttpSession session = request.getSession(true);

            CouponsDAO daoCoupon = new CouponsDAO();
            CategoryDAO daoCategory = new CategoryDAO();
            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();
            PaymentMethodDAO daoPaymentMethod = new PaymentMethodDAO();
            UserOrderDAO daoUserOrder = new UserOrderDAO();
            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();

            if (pageParam != null) {
                page = Integer.parseInt(pageParam);
            }

            Date now = formater.parse(dateNow);

            daoUserOrder.getAllListOrder();
            List<UserOrderDTO> listUserOrder = daoUserOrder.ListAllOrder();
            if (listUserOrder != null) {
                for (UserOrderDTO userOrderDTO : listUserOrder) {
                    String id = userOrderDTO.getOrderID();
                    String dateDeliveryTypeString = userOrderDTO.getDateDelivery();
                    Date dateDelivery = formater.parse(dateDeliveryTypeString);
                    if (now.compareTo(dateDelivery) >= 0 && !userOrderDTO.getStatus().equalsIgnoreCase("Ca")) {
                        daoUserOrder.updateStatusToFinish(id);
                    }
                    if (now.compareTo(dateDelivery) < 0 && !userOrderDTO.getStatus().equalsIgnoreCase("Ca")) {
                        daoUserOrder.updateStatusToConfirm(id);
                    }
                }
            }

            daoCoupon.getListCoupons();
            List<CouponsDTO> listCoupons = daoCoupon.getTheList();
            if (listCoupons != null) {
                for (CouponsDTO listCoupon : listCoupons) {
                    String expDate = listCoupon.getExpirationDate().toString();
                    Date expDateType = formater.parse(expDate);
                    if (expDateType.compareTo(now) < 0) {
                        daoCoupon.updateStatusCoupon(listCoupon.getCouponsID());
                    }
                }
            }

            daoCategory.getFullCategory();
            List<CategoryDTO> listCategory = daoCategory.getListAllCategory();
            context.setAttribute("CATEGORYLIST", listCategory);

            daoPaymentMethod.getListPaymentMethod();
            List<PaymentMethodDTO> listPayment = daoPaymentMethod.ListOrderOfUser();
            context.setAttribute("LISTPAYMENTMETHOD", listPayment);

            daoFoodOrDrink.getSuggestFood();
            List<FoodOrDrinkDTO> listSuggest = daoFoodOrDrink.getSuggestList();
            session.setAttribute("SUGGESTLIST", listSuggest);

            daoFoodOrDrink.getAllItem((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
            List<FoodOrDrinkDTO> listItem = daoFoodOrDrink.getListAllFoodOrDrink();
            context.setAttribute("ITEMLIST", listItem);

            noOfRecord = daoFoodOrDrink.countFoodForCustomer();
            if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                session.setAttribute("NOOFPAGE", noOfPage);
                session.setAttribute("CURRENTPAGE", page);
            }

            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                String userID = "";
                String password = "";
                for (Cookie cooky : cookies) {
                    if (cooky.getName().equals("USERID")) {
                        userID = cooky.getValue();
                    }
                    if (cooky.getName().equals("PASSWORD")) {
                        password = cooky.getValue();
                    }
                }

                MessageDigest md = MessageDigest.getInstance(SECURE_HASH_ALGORITHM);
                byte[] digest = md.digest(password.getBytes(StandardCharsets.UTF_8));
                String passwordEncrypte = DatatypeConverter.printHexBinary(digest).toLowerCase();
                AccountDTO dtoAccount = daoAccount.getAccount(userID, passwordEncrypte);
                if (dtoAccount != null) {
                    String roleID = daoAccount.getRole(dtoAccount.getUserID());
                    if (roleID != null && !roleID.equals("")) {
                        String roleName = daoRole.getRoleName(roleID);
                        if (roleName != null && !roleName.equals("")) {
                            if (roleName.equalsIgnoreCase("Admin")) {
                                url = ADMIN_PAGE;
                                context.setAttribute("ITEMLISTFORADMIN", null);
                                listItem.clear();
                                daoFoodOrDrink.getAllItemForAdmin((page - 1) * RECORD_PER_PAGE, RECORD_PER_PAGE);
                                listItem = daoFoodOrDrink.getListAllFoodOrDrink();
                                context.setAttribute("ITEMLISTFORADMIN", listItem);

                                noOfRecord = daoFoodOrDrink.countFoodForAdmin();
                                if (noOfRecord > 0 && RECORD_PER_PAGE > 0) {
                                    int noOfPage = (int) Math.ceil(noOfRecord * 1.0 / RECORD_PER_PAGE);
                                    session.setAttribute("NOOFPAGEFORADMIN", noOfPage);
                                    session.setAttribute("CURRENTPAGEFORADMIN", page);
                                }
                            } else if (roleName.equalsIgnoreCase("Customer")) {
                                if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                    url = VERIFY_PAGE;
                                } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                    url = CUSTOMER_PAGE;
                                }
                            }
                            session.setAttribute("ACCOUNTOBJECT", dtoAccount);
                            session.setAttribute("ROLENAME", roleName);
                        }
                    }
                }
            }
        } catch (SQLException ex) {
            log("StartUpServlet_SQL " + ex.getMessage());
        } catch (NamingException ex) {
            log("StartUpServlet_Naming " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("StartUpServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("StartUpServlet_NumberFormat " + ex.getMessage());
        } catch (ParseException ex) {
            log("StartUpServlet_Parse " + ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            log("StartUpServlet_NoSuchAlgorithm " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
