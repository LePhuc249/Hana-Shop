/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Cart.Cart;
import phucldh.Category.CategoryDAO;
import phucldh.Coupons.CouponsDAO;
import phucldh.Coupons.CouponsDTO;
import phucldh.FoodOrDrink.FoodOrDrinkDAO;
import phucldh.FoodOrDrink.FoodOrDrinkDTO;
import phucldh.OrderDetail.OrderDetailDAO;
import phucldh.OrderDetail.OrderDetailDTO;
import phucldh.Role.RoleDAO;
import phucldh.UserOrder.UserOrderDAO;
import phucldh.UserOrder.UserOrderDTO;

/**
 *
 * @author Admin
 */
@WebServlet(name = "OrderServlet", urlPatterns = {"/OrderServlet"})
public class OrderServlet extends HttpServlet {

    private final String VIEW_CART_PAGE = "viewCartPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String ADMIN_PAGE = "forAdminPage";
    private final String ERROR_ROLE_CONTENT = "Your account role can't use this function";
    private final String CUSTOMER_PAGE = "forCustomerPage";
    private final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String url = VIEW_CART_PAGE;
        String errorContent = "There have the error to order please check it again";
        String totalNow = request.getParameter("txtTotalPrice");

        boolean error = false;

        SimpleDateFormat formater = new SimpleDateFormat(DATE_FORMAT);

        try {
            ServletContext context = getServletContext();

            HttpSession session = request.getSession(false);

            AccountDAO daoAccount = new AccountDAO();
            RoleDAO daoRole = new RoleDAO();
            UserOrderDAO daoUserOrder = new UserOrderDAO();
            CategoryDAO daoCategory = new CategoryDAO();
            FoodOrDrinkDAO daoFoodOrDrink = new FoodOrDrinkDAO();
            CouponsDAO daoCoupons = new CouponsDAO();
            OrderDetailDAO daoOrderDetail = new OrderDetailDAO();

            AccountDTO dtoAccount = (AccountDTO) session.getAttribute("ACCOUNTOBJECT");
            if (dtoAccount == null) {
                url = LOGIN_PAGE;
            } else {
                String roleID = daoAccount.getRole(dtoAccount.getUserID());
                if (roleID != null && !roleID.equals("")) {
                    String roleName = daoRole.getRoleName(roleID);
                    if (roleName != null && !roleName.equals("")) {
                        if (roleName.equalsIgnoreCase("Admin")) {
                            url = ADMIN_PAGE;
                            request.setAttribute("ERRORROLE", ERROR_ROLE_CONTENT);
                        } else if (roleName.equalsIgnoreCase("Customer")) {
                            if (dtoAccount.getStatus().equalsIgnoreCase("New")) {
                                errorContent = "Account haven't Active";
                                request.setAttribute("NEWACCOUNTSESSION", errorContent);
                            } else if (dtoAccount.getStatus().equalsIgnoreCase("Active")) {
                                String couponsCode = (String) session.getAttribute("VALUETRANSFERCOUPONSCODE");
                                totalNow = (String) session.getAttribute("VALUETRANSFERTOTALPRICE");
                                String userID = dtoAccount.getUserID();
                                String orderID = null;
                                CouponsDTO dtoCoupons = null;
                                orderID = daoUserOrder.getLastOrderByIDByDate();
                                if (orderID == null) {
                                    orderID = "O_1";
                                } else if (orderID != null) {
                                    String numberCount = orderID.substring(2);
                                    int count = Integer.parseInt(numberCount);
                                    count = count + 1;
                                    orderID = "O_" + count;
                                }
                                String statusDefault = "Pr";
                                if (couponsCode != null) {
                                    dtoCoupons = daoCoupons.getCouponsByCode(couponsCode);
                                    if (dtoCoupons != null) {
                                        String couponsID = dtoCoupons.getCouponsID();
                                        daoUserOrder.getListCoupons();
                                        List<String> listCouponsHaveUsed = daoUserOrder.ListCouponsHaveUsed();
                                        if (listCouponsHaveUsed != null) {
                                            for (String string : listCouponsHaveUsed) {
                                                if (string != null) {
                                                    if (string.equalsIgnoreCase(couponsID)) {
                                                        error = true;
                                                        errorContent = "The coupon code have been used!!!!";
                                                    }
                                                }
                                            }
                                        }
                                        if (error) {
                                            request.setAttribute("ERRORCHECKCOUPONS", errorContent);
                                        } else {
                                            Cart shoppingCart = (Cart) session.getAttribute("shoppingCart");
                                            if (shoppingCart != null) {
                                                List<String> listIDItemDelete = new ArrayList<>();
                                                List<String> listNameItemDelete = new ArrayList<>();
                                                int countErrorItem = 0;
                                                for (FoodOrDrinkDTO value : shoppingCart.getShoppingCart().values()) {
                                                    String itemID = value.getItemID();
                                                    if (itemID != null) {
                                                        String itemStatus = daoFoodOrDrink.getStatusItem(itemID);
                                                        if (itemStatus != null) {
                                                            if (itemStatus.equalsIgnoreCase("Inactive")) {
                                                                countErrorItem++;
                                                                listIDItemDelete.add(itemID);
                                                            }
                                                        }
                                                    }
                                                }
                                                for (String string : listIDItemDelete) {
                                                    String name = daoFoodOrDrink.getNameByID(string);
                                                    listNameItemDelete.add(name);
                                                }
                                                if (countErrorItem > 0) {
                                                    error = true;
                                                    errorContent = "Item " + listNameItemDelete.toString() + " in cart haven't been exist any more ! Please choose other item!";
                                                } else {
                                                    float total = Float.parseFloat(totalNow);
                                                    Date now = new Date();
                                                    Calendar c = Calendar.getInstance();
                                                    c.setTime(now);
                                                    c.add(Calendar.DATE, 3);
                                                    Date currentDatePlusOne = c.getTime();
                                                    String dateDelivery = formater.format(currentDatePlusOne);
                                                    String paymentMethod = (String) session.getAttribute("PAYMENTMETHODCHOOOSE");
                                                    UserOrderDTO dtoUserOrder = new UserOrderDTO(orderID, userID, dateDelivery, paymentMethod, total, statusDefault, couponsID);
                                                    boolean createNewOrder = daoUserOrder.createNewOrder(dtoUserOrder);
                                                    if (createNewOrder) {
                                                        String lastOrderDetaiID = daoOrderDetail.getLastOrderDetailByIDByDate();
                                                        for (FoodOrDrinkDTO value : shoppingCart.getShoppingCart().values()) {
                                                            if (lastOrderDetaiID == null) {
                                                                lastOrderDetaiID = "OL_1";
                                                            } else if (lastOrderDetaiID != null) {
                                                                String numberCount = lastOrderDetaiID.substring(3);
                                                                int count = Integer.parseInt(numberCount);
                                                                count = count + 1;
                                                                lastOrderDetaiID = "OL_" + count;
                                                            }
                                                            int amount = value.getQuantity();
                                                            float price = value.getPrice();
                                                            String itemID = value.getItemID();
                                                            if (itemID != null) {
                                                                int counter = daoFoodOrDrink.getCounterItem(value.getItemID());
                                                                if (counter == 0) {
                                                                    counter = 1;
                                                                    daoFoodOrDrink.updateCounter(counter, value.getItemID());
                                                                } else if (counter > 0) {
                                                                    counter = counter + 1;
                                                                    daoFoodOrDrink.updateCounter(counter, value.getItemID());
                                                                }
                                                                String itemProperty = value.getItemName();
                                                                OrderDetailDTO dtoOrderDetail = new OrderDetailDTO(lastOrderDetaiID, orderID, itemID, amount, price, itemProperty);
                                                                boolean orderDetailCreate = daoOrderDetail.createOrderDetail(dtoOrderDetail);
                                                                if (orderDetailCreate) {
                                                                    int currentAmount = daoFoodOrDrink.getTotalQuantityItemByID(value.getItemID());
                                                                    int updateAmount = currentAmount - amount;
                                                                    daoFoodOrDrink.updateQuantityItem(itemID, updateAmount);
                                                                }
                                                            }
                                                        }
                                                        url = CUSTOMER_PAGE;
                                                        shoppingCart = null;
                                                        session.setAttribute("shoppingCart", shoppingCart);
                                                        session.setAttribute("COUPONSDTOINPUT", null);
                                                        session.setAttribute("VALUETRANSFERCOUPONSCODE", null);
                                                        session.setAttribute("VALUETRANSFERTOTALPRICE", null);
                                                    } else {
                                                        error = true;
                                                        errorContent = "There having error to order Please try again!!!!";
                                                    }
                                                }
                                            } else {
                                                error = true;
                                                errorContent = "There having error to order Please try again!!!!";
                                            }
                                            if (error) {
                                                request.setAttribute("ERRORORDER", errorContent);
                                            }
                                        }
                                    }
                                } else {
                                    Cart shoppingCart = (Cart) session.getAttribute("shoppingCart");
                                    if (shoppingCart != null) {
                                        List<String> listIDItemDelete = new ArrayList<>();
                                        List<String> listNameItemDelete = new ArrayList<>();
                                        int countErrorItem = 0;
                                        for (FoodOrDrinkDTO value : shoppingCart.getShoppingCart().values()) {
                                            String itemID = value.getItemID();
                                            if (itemID != null) {
                                                String itemStatus = daoFoodOrDrink.getStatusItem(itemID);
                                                if (itemStatus != null) {
                                                    if (itemStatus.equalsIgnoreCase("Inactive")) {
                                                        countErrorItem++;
                                                        listIDItemDelete.add(itemID);
                                                    }
                                                }
                                            }
                                        }
                                        for (String string : listIDItemDelete) {
                                            String name = daoFoodOrDrink.getNameByID(string);
                                            listNameItemDelete.add(name);
                                        }
                                        if (countErrorItem > 0) {
                                            error = true;
                                            errorContent = "Item " + listNameItemDelete.toString() + " in cart haven't been exist any more ! Please choose other item!";
                                        } else {
                                            float total = Float.parseFloat(totalNow);
                                            Date now = new Date();
                                            Calendar c = Calendar.getInstance();
                                            c.setTime(now);
                                            c.add(Calendar.DATE, 3);
                                            Date currentDatePlusOne = c.getTime();
                                            String dateDelivery = formater.format(currentDatePlusOne);
                                            String paymentMethod = (String) session.getAttribute("PAYMENTMETHODCHOOOSE");
                                            UserOrderDTO dtoUserOrder = new UserOrderDTO(orderID, userID, dateDelivery, paymentMethod, total, statusDefault);
                                            boolean createNewOrder = daoUserOrder.createNewOrder(dtoUserOrder);
                                            if (createNewOrder) {
                                                String lastOrderDetaiID = daoOrderDetail.getLastOrderDetailByIDByDate();
                                                for (FoodOrDrinkDTO value : shoppingCart.getShoppingCart().values()) {
                                                    if (lastOrderDetaiID == null) {
                                                        lastOrderDetaiID = "OL_1";
                                                    } else if (lastOrderDetaiID != null) {
                                                        String numberCount = lastOrderDetaiID.substring(3);
                                                        int count = Integer.parseInt(numberCount);
                                                        count = count + 1;
                                                        lastOrderDetaiID = "OL_" + count;
                                                    }
                                                    int amount = value.getQuantity();
                                                    float price = value.getPrice();
                                                    String itemID = value.getItemID();
                                                    int counter = daoFoodOrDrink.getCounterItem(value.getItemID());
                                                    if (counter == 0) {
                                                        counter = 1;
                                                        daoFoodOrDrink.updateCounter(counter, value.getItemID());
                                                    } else if (counter > 0) {
                                                        counter = counter + 1;
                                                        daoFoodOrDrink.updateCounter(counter, value.getItemID());
                                                    }
                                                    String itemProperty = value.getItemName();
                                                    OrderDetailDTO dtoOrderDetail = new OrderDetailDTO(lastOrderDetaiID, orderID, itemID, amount, price, itemProperty);
                                                    boolean orderDetailCreate = daoOrderDetail.createOrderDetail(dtoOrderDetail);
                                                    if (orderDetailCreate) {
                                                        int currentAmount = daoFoodOrDrink.getTotalQuantityItemByID(value.getItemID());
                                                        int updateAmount = currentAmount - amount;
                                                        daoFoodOrDrink.updateQuantityItem(itemID, updateAmount);
                                                    }
                                                }
                                                url = CUSTOMER_PAGE;
                                                shoppingCart = null;
                                                session.setAttribute("shoppingCart", shoppingCart);
                                                session.setAttribute("COUPONSDTOINPUT", null);
                                                session.setAttribute("VALUETRANSFERCOUPONSCODE", null);
                                                session.setAttribute("VALUETRANSFERTOTALPRICE", null);
                                            } else {
                                                error = true;
                                                errorContent = "There having error to order Please try again!!!!";
                                            }
                                        }
                                    } else {
                                        error = true;
                                        errorContent = "Cart not exist! Please create new cart by add item";
                                    }
                                    if (error) {
                                        request.setAttribute("ERRORORDER", errorContent);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("OrderServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            log("OrderServlet_SQL " + ex.getMessage());
        } catch (NullPointerException ex) {
            log("OrderServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("OrderServlet_NumberFormat " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
