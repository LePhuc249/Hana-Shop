/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.mail.MessagingException;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import phucldh.Account.AccountCreateError;
import phucldh.Account.AccountDAO;
import phucldh.Account.AccountDTO;
import phucldh.Mail.SendEmail;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CreateAccountServlet", urlPatterns = {"/CreateAccountServlet"})
public class CreateAccountServlet extends HttpServlet {

    private final String CREATE_ACCOUNT_ERROR_PAGE = "createAccountErrorPage";
    private final String LOGIN_PAGE = "loginPage";
    private final String EMAIL_PATTERN = "\\w+@\\w+[.]\\w+";
    private final String DEFAULT_ROLE = "CT";
    private final String SECURE_HASH_ALGORITHM = "SHA-256";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String url = CREATE_ACCOUNT_ERROR_PAGE;
        String username = request.getParameter("txtUsername");
        String password = request.getParameter("txtPassword");
        String confirm = request.getParameter("txtConfirm");
        String fullname = request.getParameter("txtFullname");
        String email = request.getParameter("txtEmail");
        String phone = request.getParameter("txtPhone");
        String address = request.getParameter("txtAddress");
        
        boolean error = false;
        
        AccountCreateError errors = new AccountCreateError();

        try {
            HttpSession session = request.getSession(true);
            
            AccountDAO daoAccount = new AccountDAO();

            if (username.trim().length() < 6 || username.trim().length() > 30) {
                error = true;
                errors.setAccountIDLengthError("Username must have length from 6-30 character");
            }
            
            if (password != null) {
                if (password.trim().length() < 6 || password.trim().length() > 50) {
                    error = true;
                    errors.setPasswordLengthError("Password must have length from 6-50 character");
                }
            }
            
            if (!confirm.trim().equals(password.trim())) {
                error = true;
                errors.setConfirmNotMatch("Password and Confirm must same");
            }
            
            if (fullname.trim().length() < 2 || fullname.trim().length() > 50) {
                error = true;
                errors.setFullnameLengthError("Fullname must have length from 2-50 character");
            }
            
            if (email.trim().length() < 2 || email.trim().length() > 50) {
                error = true;
                errors.setEmailLengthError("Email must have length from 2-50 character");
            }
            
            boolean check = email.matches(EMAIL_PATTERN);
            if (!check) {
                error = true;
                errors.setEmailTypeErr("Email must have true type");
            }
            
            if (phone.trim().length() < 2 || phone.trim().length() > 20) {
                error = true;
                errors.setPhoneLengthErr("Phone must have length from 2-20 character");
            }
            
            if (address.trim().length() < 2 || address.trim().length() > 200) {
                error = true;
                errors.setAddressLengthErr("Address must have length from 2-200 character");
            }
            
            if (error) {
                request.setAttribute("INSERTERR", errors);
            } else {
                MessageDigest md = MessageDigest.getInstance(SECURE_HASH_ALGORITHM);
                byte[] digest = md.digest(password.getBytes(StandardCharsets.UTF_8));
                String passwordEncrypte = DatatypeConverter.printHexBinary(digest).toLowerCase();
                AccountDTO dtoAccount = new AccountDTO(username, passwordEncrypte, fullname, email, phone, address);
                boolean result = daoAccount.createAccount(dtoAccount);
                if (result) {
                    SendEmail sm = new SendEmail();
                    String codeVerify = sm.getCodeRandom();
                    if (codeVerify != null) {
                        session.setAttribute("VERIFYCODE", codeVerify);
                        boolean send = sm.sendEmail(dtoAccount, codeVerify);
                        if (send) {
                            url = LOGIN_PAGE;
                        }
                    }
                }
            }
        } catch (NamingException ex) {
            log("CreateNewAccountServlet_Naming " + ex.getMessage());
        } catch (SQLException ex) {
            String errorMsg = ex.getMessage();
            log("CreateNewAccountServlet_SQL " + error);
            if (errorMsg.contains("duplicate")) {
                errors.setAccountIDIsExist("Email: " + email + " have been exists");
                request.setAttribute("INSERTERR", errors);
            }
        } catch (NullPointerException ex) {
            log("CreateNewAccountServlet_NullPointer " + ex.getMessage());
        } catch (NumberFormatException ex) {
            log("CreateNewAccountServlet_NumberFormat " + ex.getMessage());
        } catch (NoSuchAlgorithmException ex) {
            log("CreateNewAccountSerlvet_NoSuchAlgorithm " + ex.getMessage());
        } catch (MessagingException ex) {
            log("CreateNewAccountSerlvet_Messaging " + ex.getMessage());
        } finally {
            RequestDispatcher rd = request.getRequestDispatcher(url);
            rd.forward(request, response);
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
