/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.FoodOrDrink;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class FoodOrDrinkDTO implements Serializable {

    private String itemID;
    private String itemName;
    private String img;
    private String description;
    private float price;
    private Date createDate;
    private String productName;
    private int quantity;
    private String status;
    private String categoryID;
    private int counter;

    public FoodOrDrinkDTO(String itemID, String itemName, String img, String description, float price, String productName, int quantity, String status, String categoryID) {
        this.itemID = itemID;
        this.itemName = itemName;
        this.img = img;
        this.description = description;
        this.price = price;
        this.productName = productName;
        this.quantity = quantity;
        this.status = status;
        this.categoryID = categoryID;
    }

    public FoodOrDrinkDTO(String itemID, String itemName, String img, String description, float price, String productName, int quantity, String status, String categoryID, int counter) {
        this.itemID = itemID;
        this.itemName = itemName;
        this.img = img;
        this.description = description;
        this.price = price;
        this.productName = productName;
        this.quantity = quantity;
        this.status = status;
        this.categoryID = categoryID;
        this.counter = counter;
    }

    public FoodOrDrinkDTO(String itemID, String itemName, String img, String description, float price, Date createDate, String productName, int quantity, String status, String categoryID, int counter) {
        this.itemID = itemID;
        this.itemName = itemName;
        this.img = img;
        this.description = description;
        this.price = price;
        this.createDate = createDate;
        this.productName = productName;
        this.quantity = quantity;
        this.status = status;
        this.categoryID = categoryID;
        this.counter = counter;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

}
