/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.FoodOrDrink;

/**
 *
 * @author Admin
 */
public class FoodOrDrinkCreateError {

    private String nameLengthError;
    private String imageError;
    private String descriptionLengthError;
    private String priceError;
    private String productNameLengthError;
    private String quantityError;
    private String categoryError;
    private String noThingChange;
    private String counterError;
    private String statusError;

    public FoodOrDrinkCreateError() {
    }

    public FoodOrDrinkCreateError(String nameLengthError, String imageError, String descriptionLengthError, String priceError, String productNameLengthError, String quantityError, String categoryError, String noThingChange, String counterError, String statusError) {
        this.nameLengthError = nameLengthError;
        this.imageError = imageError;
        this.descriptionLengthError = descriptionLengthError;
        this.priceError = priceError;
        this.productNameLengthError = productNameLengthError;
        this.quantityError = quantityError;
        this.categoryError = categoryError;
        this.noThingChange = noThingChange;
        this.counterError = counterError;
        this.statusError = statusError;
    }

    public String getStatusError() {
        return statusError;
    }

    public void setStatusError(String statusError) {
        this.statusError = statusError;
    }

    public String getImageError() {
        return imageError;
    }

    public void setImageError(String imageError) {
        this.imageError = imageError;
    }

    public String getCounterError() {
        return counterError;
    }

    public void setCounterError(String counterError) {
        this.counterError = counterError;
    }

    public String getNoThingChange() {
        return noThingChange;
    }

    public void setNoThingChange(String noThingChange) {
        this.noThingChange = noThingChange;
    }

    public String getNameLengthError() {
        return nameLengthError;
    }

    public void setNameLengthError(String nameLengthError) {
        this.nameLengthError = nameLengthError;
    }

    public String getDescriptionLengthError() {
        return descriptionLengthError;
    }

    public void setDescriptionLengthError(String descriptionLengthError) {
        this.descriptionLengthError = descriptionLengthError;
    }

    public String getPriceError() {
        return priceError;
    }

    public void setPriceError(String priceError) {
        this.priceError = priceError;
    }

    public String getProductNameLengthError() {
        return productNameLengthError;
    }

    public void setProductNameLengthError(String productNameLengthError) {
        this.productNameLengthError = productNameLengthError;
    }

    public String getQuantityError() {
        return quantityError;
    }

    public void setQuantityError(String quantityError) {
        this.quantityError = quantityError;
    }

    public String getCategoryError() {
        return categoryError;
    }

    public void setCategoryError(String categoryError) {
        this.categoryError = categoryError;
    }

}
