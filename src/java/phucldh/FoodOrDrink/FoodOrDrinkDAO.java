/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.FoodOrDrink;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class FoodOrDrinkDAO implements Serializable {

    private List<FoodOrDrinkDTO> listFoodOrDrink;
    private List<FoodOrDrinkDTO> listAllFoodOrDrink;
    private List<FoodOrDrinkDTO> suggestList;

    public List<FoodOrDrinkDTO> getListFoodOrDrink() throws NamingException, SQLException {
        return listFoodOrDrink;
    }

    public List<FoodOrDrinkDTO> getListAllFoodOrDrink() throws NamingException, SQLException {
        return listAllFoodOrDrink;
    }

    public List<FoodOrDrinkDTO> getSuggestList() throws NamingException, SQLException {
        return suggestList;
    }

    public boolean checkExistFood(String img, String name, String descript, String product) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE img = ? AND itemName = ? AND description = ? "
                        + "AND productName = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, img);
                stm.setString(2, name);
                stm.setString(3, descript);
                stm.setString(4, product);
                rs = stm.executeQuery();
                if (rs.next()) {
                    return true;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public FoodOrDrinkDTO searchFoodByID(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public FoodOrDrinkDTO searchFoodByIDToAdd(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemID = ? AND status = 'Active' AND "
                        + "quantity > 0";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    return dto;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public void getAllItem(int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price,  "
                        + "productName, quantity, status, categoryID, counter  FROM FoodOrDrink "
                        + "WHERE status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setInt(1, offset);
                stm.setInt(2, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listAllFoodOrDrink == null) {
                        this.listAllFoodOrDrink = new ArrayList<>();
                    }
                    this.listAllFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByName(String keyword, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "AND status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                stm.setInt(2, offset);
                stm.setInt(3, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByNameAndCategory(String name, String category, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter  "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? AND categoryID = ? "
                        + "AND status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, category);
                stm.setInt(3, offset);
                stm.setInt(4, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByNameAndPrice(String name, String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter  "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? AND (price < ? AND price >=?) "
                        + "AND status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                stm.setInt(4, offset);
                stm.setInt(5, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByCategory(String keyword, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter  "
                        + "FROM FoodOrDrink WHERE categoryID = ? "
                        + "AND status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setInt(2, offset);
                stm.setInt(3, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByCategoryAndPrice(String keyword, String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter  "
                        + "FROM FoodOrDrink WHERE categoryID = ? AND (price < ? AND price >=?) "
                        + "AND status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                stm.setInt(4, offset);
                stm.setInt(5, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByLimitPrice(String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter  "
                        + "FROM FoodOrDrink WHERE (price < ? AND price >=?) "
                        + "AND status = 'Active' AND quantity > 0 "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, maxPrice);
                stm.setString(2, minPrice);
                stm.setInt(3, offset);
                stm.setInt(4, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByAllFilter(String keyword, String category, String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter  "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "AND status = 'Active' AND quantity > 0 AND categoryID = ? AND (price < ? AND price >=?) "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                stm.setString(2, category);
                stm.setString(3, maxPrice);
                stm.setString(4, minPrice);
                stm.setInt(5, offset);
                stm.setInt(6, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void getAllItemForAdmin(int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter FROM FoodOrDrink "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setInt(1, offset);
                stm.setInt(2, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listAllFoodOrDrink == null) {
                        this.listAllFoodOrDrink = new ArrayList<>();
                    }
                    this.listAllFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByNameForAdmin(String keyword, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price,  "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                stm.setInt(2, offset);
                stm.setInt(3, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByNameAndCategoryForAdmin(String name, String category, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price,  "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? AND categoryID = ? "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, category);
                stm.setInt(3, offset);
                stm.setInt(4, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByNameAndPriceForAdmin(String name, String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? AND (price < ? AND price >=?) "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                stm.setInt(4, offset);
                stm.setInt(5, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByCategoryForAdmin(String keyword, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE categoryID = ? "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setInt(2, offset);
                stm.setInt(3, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByCategoryAndPriceForAdmin(String keyword, String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE categoryID = ? AND (price < ? AND price >=?) "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                stm.setInt(4, offset);
                stm.setInt(5, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByLimitPriceForAdmin(String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE (price < ? AND price >=?) "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, maxPrice);
                stm.setString(2, minPrice);
                stm.setInt(3, offset);
                stm.setInt(4, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void searchByAllFilterForAdmin(String keyword, String category, String minPrice, String maxPrice, int offset, int fetch) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "AND categoryID = ? AND (price < ? AND price >=?) "
                        + "Order by createDate DESC "
                        + "Offset ? Rows Fetch Next ? Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                stm.setString(2, category);
                stm.setString(3, maxPrice);
                stm.setString(4, minPrice);
                stm.setInt(5, offset);
                stm.setInt(6, fetch);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.listFoodOrDrink == null) {
                        this.listFoodOrDrink = new ArrayList<>();
                    }
                    this.listFoodOrDrink.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public int countFoodForCustomer() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink "
                        + "WHERE status = 'Active' AND quantity > 0";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countFoodForAdmin() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByName(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "AND status = 'Active' AND quantity > 0 ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByNameAndCategory(String name, String category) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? AND categoryID = ? "
                        + "AND status = 'Active' AND quantity > 0 ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, category);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByNameAndPrice(String name, String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? AND (price < ? AND price >=?) "
                        + "AND status = 'Active' AND quantity > 0 ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByCategory(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE categoryID = ? "
                        + "AND status = 'Active' AND quantity > 0 ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByCategoryAndPrice(String keyword, String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE categoryID = ? AND (price < ? AND price >=?) "
                        + "AND status = 'Active' AND quantity > 0 ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByLimitPrice(String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE (price < ? AND price >=?) "
                        + "AND status = 'Active' AND quantity > 0 ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, maxPrice);
                stm.setString(2, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByAllFilter(String keyword, String category, String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "AND status = 'Active' AND quantity > 0 AND categoryID = ? AND (price < ? AND price >=?) ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                stm.setString(2, category);
                stm.setString(3, maxPrice);
                stm.setString(4, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByNameForAdmin(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByNameAndCategoryForAdmin(String name, String category) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? AND categoryID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, category);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByNameAndPriceForAdmin(String name, String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? AND (price < ? AND price >=?) ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + name + "%");
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByCategoryForAdmin(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE categoryID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByCategoryAndPriceForAdmin(String keyword, String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE categoryID = ? AND (price < ? AND price >=?) ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                stm.setString(2, maxPrice);
                stm.setString(3, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByLimitPriceForAdmin(String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE (price < ? AND price >=?) ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, maxPrice);
                stm.setString(2, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int countRecordSearchByAllFilterForAdmin(String keyword, String category, String minPrice, String maxPrice) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            int count = 0;
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE itemName LIKE ? "
                        + "AND categoryID = ? AND (price < ? AND price >=?) ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, "%" + keyword + "%");
                stm.setString(2, category);
                stm.setString(3, maxPrice);
                stm.setString(4, minPrice);
                rs = stm.executeQuery();
                while (rs.next()) {
                    count++;
                }
                return count;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public boolean updateQuantityItem(String keyword, int quantity) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update FoodOrDrink SET quantity = ? Where itemID = ?";
                stm = conn.prepareStatement(sql);
                stm.setInt(1, quantity);
                stm.setString(2, keyword);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public int getTotalQuantityItemByID(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int quantity = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT quantity FROM FoodOrDrink WHERE itemID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    quantity = rs.getInt("quantity");
                }
                return quantity;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public int getCounterItem(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;

        int counter;

        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT counter FROM FoodOrDrink WHERE itemID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    counter = rs.getInt("counter");
                    return counter;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return 0;
    }

    public boolean updateCounter(int counter, String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update FoodOrDrink SET counter = ? WHERE itemID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setInt(1, counter);
                stm.setString(2, keyword);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public void getSuggestFood() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE quantity > 0 "
                        + "AND status = 'Active' "
                        + "Order by counter DESC "
                        + "Offset 0 Rows Fetch Next 5 Rows Only ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.suggestList == null) {
                        this.suggestList = new ArrayList<>();
                    }
                    this.suggestList.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void getSuggestFoodByCategory(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price, "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE quantity > 0 "
                        + "AND status = 'Active' AND categoryID = ? "
                        + "Order by counter DESC "
                        + "Offset 0 Rows Fetch Next 2 Rows Only ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    FoodOrDrinkDTO dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                    if (this.suggestList == null) {
                        this.suggestList = new ArrayList<>();
                    }
                    this.suggestList.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public String getLastItemID() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String id;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID FROM FoodOrDrink WHERE createDate = (SELECT MAX(createDate) FROM FoodOrDrink) ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                if (rs.next()) {
                    id = rs.getString("itemID");
                    return id;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public String getNameByID(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String name = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemName FROM FoodOrDrink WHERE itemID = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    name = rs.getString("itemName");
                    return name;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return name;
    }

    public int getTotalQuantityItem(String keyword) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        int quantity = 0;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT quantity FROM FoodOrDrink WHERE itemID = ? and status='Active'";
                stm = conn.prepareStatement(sql);
                stm.setString(1, keyword);
                rs = stm.executeQuery();
                if (rs.next()) {
                    quantity = rs.getInt("quantity");
                    return quantity;
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return quantity;
    }

    public boolean deleteItem(String itemId) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "UPDATE FoodOrDrink SET status = 'Inactive' WHERE itemName = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, itemId);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean updateItem(FoodOrDrinkDTO dtoFoodOrDrink) throws SQLException, NamingException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "UPDATE FoodOrDrink SET itemName = ?, img = ?, description = ?, "
                        + "price = ?, productName = ?, quantity = ?, status = ?, "
                        + "categoryID = ?, counter = ? WHERE itemID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoFoodOrDrink.getItemName());
                stm.setString(2, dtoFoodOrDrink.getImg());
                stm.setString(3, dtoFoodOrDrink.getDescription());
                stm.setFloat(4, dtoFoodOrDrink.getPrice());
                stm.setString(5, dtoFoodOrDrink.getProductName());
                stm.setInt(6, dtoFoodOrDrink.getQuantity());
                stm.setString(7, dtoFoodOrDrink.getStatus());
                stm.setString(8, dtoFoodOrDrink.getCategoryID());
                stm.setInt(9, dtoFoodOrDrink.getCounter());
                stm.setString(10, dtoFoodOrDrink.getItemID());
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public boolean insertItem(FoodOrDrinkDTO dtoFoodOrDrink) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Insert into FoodOrDrink (itemID, itemName, img, description, price,  "
                        + "productName, quantity, status,categoryID, counter) Values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                stm = conn.prepareStatement(sql);
                stm.setString(1, dtoFoodOrDrink.getItemID());
                stm.setString(2, dtoFoodOrDrink.getItemName());
                stm.setString(3, dtoFoodOrDrink.getImg());
                stm.setString(4, dtoFoodOrDrink.getDescription());
                stm.setFloat(5, dtoFoodOrDrink.getPrice());
                stm.setString(6, dtoFoodOrDrink.getProductName());
                stm.setInt(7, dtoFoodOrDrink.getQuantity());
                stm.setString(8, dtoFoodOrDrink.getStatus());
                stm.setString(9, dtoFoodOrDrink.getCategoryID());
                stm.setInt(10, 0);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }

    public FoodOrDrinkDTO getFoodOrDrinkByID(String id) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        FoodOrDrinkDTO dto = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT itemID, itemName, img, description, price,  "
                        + "productName, quantity, status, categoryID, counter "
                        + "FROM FoodOrDrink WHERE itemID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String itemID = rs.getString("itemID");
                    String itemName = rs.getString("itemName");
                    String img = rs.getString("img");
                    String description = rs.getString("description");
                    float price = rs.getFloat("price");
                    String productName = rs.getString("productName");
                    int quantity = rs.getInt("quantity");
                    String status = rs.getString("status");
                    String categoryID = rs.getString("categoryID");
                    int counter = rs.getInt("counter");
                    dto = new FoodOrDrinkDTO(itemID, itemName, img, description, price, productName, quantity, status, categoryID, counter);
                }
                return dto;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public String getStatusItem(String itemID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        String status = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT status FROM FoodOrDrink WHERE itemID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, itemID);
                rs = stm.executeQuery();
                if (rs.next()) {
                    status = rs.getString("status");
                }
                return status;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

}
