/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.Coupons;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class CouponsDAO implements Serializable {

    private List<CouponsDTO> listCoupons;

    public List<CouponsDTO> getTheList() throws NamingException, SQLException {
        return listCoupons;
    }
    
    private List<CouponsDTO> listCouponsByItem;

    public List<CouponsDTO> getTheListItem() throws NamingException, SQLException {
        return listCouponsByItem;
    }

    public CouponsDTO getCouponsByCode(String codeInput) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        CouponsDTO dtoCoupons = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT couponID, couponCode, discount_amount, description, expiration_date, isDeleted "
                        + "FROM Coupons Where couponCode = ? AND isDeleted = 0";
                stm = conn.prepareStatement(sql);
                stm.setString(1, codeInput);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String id = rs.getString("couponID");
                    String code = rs.getString("couponCode");
                    int disAmount = rs.getInt("discount_amount");
                    String descript = rs.getString("description");
                    Date expDate = rs.getDate("expiration_date");
                    boolean isDelete = rs.getBoolean("isDeleted");
                    dtoCoupons = new CouponsDTO(id, code, disAmount, descript, expDate, isDelete);
                }
                return dtoCoupons;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }

    public void getListCoupons() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT couponID, couponCode, discount_amount, description, expiration_date, isDeleted FROM Coupons ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("couponID");
                    String code = rs.getString("couponCode");
                    int disAmount = rs.getInt("discount_amount");
                    String descript = rs.getString("description");
                    Date expDate = rs.getDate("expiration_date");
                    boolean isDelete = rs.getBoolean("isDeleted");
                    CouponsDTO dtoCoupons = new CouponsDTO(id, code, disAmount, descript, expDate, isDelete);
                    if (this.listCoupons == null) {
                        this.listCoupons = new ArrayList<>();
                    }
                    this.listCoupons.add(dtoCoupons);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean updateStatusCoupon(String id) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "Update Coupons set isDeleted = 1 Where couponID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                int row = stm.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }
        } finally {
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return false;
    }
    
    public void getCouponByItemID(String idInput) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT couponID, couponCode, discount_amount, description, "
                        + "expiration_date, isDeleted FROM Coupons Where description = ?";
                stm = conn.prepareStatement(sql);
                stm.setString(1, idInput);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String id = rs.getString("couponID");
                    String code = rs.getString("couponCode");
                    int disAmount = rs.getInt("discount_amount");
                    String descript = rs.getString("description");
                    Date expDate = rs.getDate("expiration_date");
                    boolean isDelete = rs.getBoolean("isDeleted");
                    CouponsDTO dtoCoupons = new CouponsDTO(id, code, disAmount, descript, expDate, isDelete);
                    if (this.listCouponsByItem == null) {
                        this.listCouponsByItem = new ArrayList<>();
                    }
                    this.listCouponsByItem.add(dtoCoupons);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
