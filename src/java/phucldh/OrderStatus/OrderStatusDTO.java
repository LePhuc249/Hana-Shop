/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.OrderStatus;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class OrderStatusDTO implements Serializable{
    private String orderStatusID;
    private String orderStatusName;
    private Date createDate;
    private boolean isDeleted;

    public OrderStatusDTO() {
    }

    public OrderStatusDTO(String orderStatusID, String orderStatusName, boolean isDeleted) {
        this.orderStatusID = orderStatusID;
        this.orderStatusName = orderStatusName;
        this.isDeleted = isDeleted;
    }

    public OrderStatusDTO(String orderStatusID, String orderStatusName, Date createDate, boolean isDeleted) {
        this.orderStatusID = orderStatusID;
        this.orderStatusName = orderStatusName;
        this.createDate = createDate;
        this.isDeleted = isDeleted;
    }

    public String getOrderStatusID() {
        return orderStatusID;
    }

    public void setOrderStatusID(String orderStatusID) {
        this.orderStatusID = orderStatusID;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    
}
