/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.PaymentMethod;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Admin
 */
public class PaymentMethodDTO implements Serializable {

    private String ID;
    private String name;
    private Date createDate;
    private boolean isDeleted;

    public PaymentMethodDTO() {
    }

    public PaymentMethodDTO(String ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public PaymentMethodDTO(String ID, String name, boolean isDeleted) {
        this.ID = ID;
        this.name = name;
        this.isDeleted = isDeleted;
    }

    public PaymentMethodDTO(String ID, String name, Date createDate, boolean isDeleted) {
        this.ID = ID;
        this.name = name;
        this.createDate = createDate;
        this.isDeleted = isDeleted;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

}
