/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.PaymentMethod;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import phucldh.DBUtils.DBHelper;

/**
 *
 * @author Admin
 */
public class PaymentMethodDAO implements Serializable {
    
    private List<PaymentMethodDTO> listPaymentMethod;

    public List<PaymentMethodDTO> ListOrderOfUser() throws NamingException, SQLException {
        return listPaymentMethod;
    }

    public PaymentMethodDTO getPaymentMethodByID(String id) throws NamingException , SQLException{
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        PaymentMethodDTO dto = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT ID, Name FROM PaymentMethod "
                        + "WHERE ID = ? ";
                stm = conn.prepareStatement(sql);
                stm.setString(1, id);
                rs = stm.executeQuery();
                if (rs.next()) {
                    String idPay = rs.getString("ID");
                    String namePay = rs.getString("Name");
                    dto = new PaymentMethodDTO(idPay, namePay);
                }
                return dto;
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return null;
    }
    
    public void getListPaymentMethod() throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = DBHelper.makeConnection();
            if (conn != null) {
                String sql = "SELECT ID, Name FROM PaymentMethod ";
                stm = conn.prepareStatement(sql);
                rs = stm.executeQuery();
                while (rs.next()) {
                    String idPay = rs.getString("ID");
                    String namePay = rs.getString("Name");
                    PaymentMethodDTO dto = new PaymentMethodDTO(idPay, namePay);
                    if (this.listPaymentMethod == null) {
                        this.listPaymentMethod = new ArrayList<>();
                    }
                    this.listPaymentMethod.add(dto);
                }
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (stm != null) {
                stm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }
}
