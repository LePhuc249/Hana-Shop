<%-- 
    Document   : viewDetailItem
    Created on : Mar 10, 2021, 2:47:07 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Item Detail Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forAdminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with customer account !!!!!
                    </font>
                </h1>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <c:set var="CurrentAccountStatus" value="${sessionScope.STATUSACCOUNT}"/>
                <c:set var="errorROLE" value="${requestScope.ERRORROLE}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="viewCartPage">View Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                </div>
                <c:if test="${CurrentAccountStatus eq 'Active'}">
                    <div>
                        <c:set var="item" value="${requestScope.ITEMSHOW}"/>
                        <c:if test="${not empty item}">
                            <div class="show-detail ">
                                <form action="addCart" method="POST" class="register-form">
                                    <input type="hidden" name="txtItemID" value="${item.itemID}"/>
                                    <div class="col-xl-6 col-lg-12 col-md-3 col-sm-6 col-xs-12 ">
                                        <img alt="itemImage" src="${item.img}" width="100" height="100"/><br/>
                                    </div>
                                    <div class="product-heading">
                                        <h2>
                                            Item Name: ${item.itemName}<br/>
                                        </h2>
                                    </div>
                                    <div class="product-detail-side"><span class="new-price">${item.price}$</span></div>
                                    Item Description: ${item.description}<br/>
                                    Item Product: ${item.productName}<br/>
                                    Quantity in store: ${item.quantity}<br/>
                                    Category ID: ${item.categoryID}<br/>
                                    <input type="hidden" name="page" value="${requestScope.PAGESEARCH}"/>
                                    <input type="hidden" name="txtSearchItem" value="${requestScope.NAMESEARCH}"/>
                                    <input type="hidden" name="txtMin" value="${requestScope.MINSEARCH}"/>
                                    <input type="hidden" name="txtMax" value="${requestScope.MAXSEARCH}"/>
                                    <input type="hidden" name="cboCategory" value="${requestScope.CATEGORYSEARCH}"/>
                                    <input class="btn btn-success" type="submit" value="AddToCart" name="btAction"/>
                                </form>
                            </div>
                            <div class="label-suggest2">
                                <h2>Suggest</h2>
                            </div>
                            <c:set var="listItemsSuggest" value="${sessionScope.SUGGESTLISTBYCATE}"/>
                            <div class="table-suggest-box2">
                                <c:if test="${not empty listItemsSuggest}">
                                    <table border="1">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Price</th>
                                                <th>Category</th>
                                                <td></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="items" items="${listItemsSuggest}" varStatus="counter">
                                                <tr>
                                                    <td>
                                                        ${items.itemName}
                                                        <input type="hidden" name="txtItemID" value="${items.itemID}"/>
                                                    </td>
                                                    <td>
                                                        <img alt="itemImage" src="${items.img}" width="100" height="100"/>
                                                    </td>
                                                    <td>
                                                        ${items.price}$
                                                    </td>
                                                    <td>
                                                        ${items.categoryID}
                                                    </td>
                                                    <td>
                                                        <c:url var="urlRewriting" value="#">
                                                            <c:param name="btAction" value="ShowDetail"/>
                                                            <c:param name="id" value="${items.itemID}"/>
                                                        </c:url>
                                                        <a href="${urlRewriting}">Show Detail</a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                            <div class="label-suggest3">
                                <h2>List Coupons</h2>
                            </div>
                            <c:set var="listCouponsSuggest" value="${requestScope.LISTCOUPONSFORITEM}"/>
                            <div class="table-suggest-box3">
                                <c:if test="${not empty listItemsSuggest}">
                                    <table border="1">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Discount Amount</th>
                                                <th>Expiration Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach var="items" items="${listCouponsSuggest}" varStatus="counter">
                                                <tr>
                                                    <td>
                                                        ${items.couponsCode}
                                                    </td>
                                                    <td>
                                                        ${items.discountAmount}%
                                                    </td>
                                                    <td>
                                                        ${items.expirationDate}
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                        </c:if>
                        <c:if test="${empty item}">
                            Please choose item to view the detail
                        </c:if>
                    </div>
                </c:if>
                <c:if test="${CurrentAccountStatus eq 'New'}">
                    <h1>
                        <font color="red">
                        Account haven't active.Please active account!
                        </font>
                    </h1>
                </c:if>    

            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="homePage">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="viewCartPage">View Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="showHistoryPage">History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <c:set var="item" value="${requestScope.ITEMSHOW}"/>
            <c:if test="${not empty item}">
                <div class="show-detail ">
                    <form action="addCart" method="POST" class="register-form">
                        <input type="hidden" name="txtItemID" value="${item.itemID}"/>
                        <div class="col-xl-6 col-lg-12 col-md-3 col-sm-6 col-xs-12 ">
                            <img alt="itemImage" src="${item.img}" width="100" height="100"/><br/>
                        </div>
                        <div class="product-heading">
                            <h2>
                                Item Name: ${item.itemName}<br/>
                            </h2>
                        </div>
                        <div class="product-detail-side"><span class="new-price">${item.price}$</span></div>
                        Item Description: ${item.description}<br/>
                        Item Product: ${item.productName}<br/>
                        Quantity in store: ${item.quantity}<br/>
                        Category ID: ${item.categoryID}<br/>
                        <input type="hidden" name="page" value="${requestScope.PAGESEARCH}"/>
                        <input type="hidden" name="txtSearchItem" value="${requestScope.NAMESEARCH}"/>
                        <input type="hidden" name="txtMin" value="${requestScope.CATEGORYSEARCH}"/>
                        <input type="hidden" name="txtMax" value="${requestScope.MINSEARCH}"/>
                        <input type="hidden" name="cboCategory" value="${requestScope.MAXSEARCH}"/>
                        <input class="btn btn-success" type="submit" value="AddToCart" name="btAction"/>
                    </form>
                </div>
                <div class="label-suggest2">
                    <h2>Suggest</h2>
                </div>
                <c:set var="listItemsSuggest" value="${sessionScope.SUGGESTLISTBYCATE}"/>
                <div class="table-suggest-box2">
                    <c:if test="${not empty listItemsSuggest}">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Price</th>
                                    <th>Category</th>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="items" items="${listItemsSuggest}" varStatus="counter">
                                    <tr>
                                        <td>
                                            ${items.itemName}
                                            <input type="hidden" name="txtItemID" value="${items.itemID}"/>
                                        </td>
                                        <td>
                                            <img alt="itemImage" src="${items.img}" width="100" height="100"/>
                                        </td>
                                        <td>
                                            ${items.price}$
                                        </td>
                                        <td>
                                            ${items.categoryID}
                                        </td>
                                        <td>
                                            <c:url var="urlRewriting" value="#">
                                                <c:param name="btAction" value="ShowDetail"/>
                                                <c:param name="id" value="${items.itemID}"/>
                                            </c:url>
                                            <a href="${urlRewriting}">Show Detail</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                </div>
                <div class="label-suggest3">
                    <h2>List Coupons</h2>
                </div>
                <c:set var="listCouponsSuggest" value="${requestScope.LISTCOUPONSFORITEM}"/>
                <div class="table-suggest-box3">
                    <c:if test="${not empty listItemsSuggest}">
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Discount Amount</th>
                                    <th>Expiration Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="items" items="${listCouponsSuggest}" varStatus="counter">
                                    <tr>
                                        <td>
                                            ${items.couponsCode}
                                        </td>
                                        <td>
                                            ${items.discountAmount}%
                                        </td>
                                        <td>
                                            ${items.expirationDate}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                </div>
            </c:if>
            <c:if test="${empty item}">
                Please choose item to view the detail
            </c:if>
        </c:if>
    </body>
</html>
