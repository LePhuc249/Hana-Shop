function goToLoginPage() {
    window.location.href = "/Lab1Filter/loginPage";
}

function goToCreateAccountPage() {
    window.location.href = "/Lab1Filter/createAccountPage?txtSearchItem=&txtMin=&txtMax=&cboCategory=NULL&btAction=SearchItem";
}

function goToSearchForGuest() {
    window.location.href = "/Lab1Filter/homePage?txtSearchItem=&txtMin=&txtMax=&cboCategory=NULL&btAction=SearchItem";
}

function goToSearchForAdminPage() {
    window.location.href = "/Lab1Filter/forAdminPage?txtSearchItem=&txtMin=&txtMax=&cboCategory=NULL&btAction=SearchItem";
}

function goToCreateItemPage() {
    window.location.href = "/Lab1Filter/createItemPage";
}

function goToForCustomerPage() {
    window.location.href = "/Lab1Filter/forCustomerPage?txtSearchItem=&txtMin=&txtMax=&cboCategory=NULL&btAction=SearchItem";
}

function goToForAdminPage() {
    window.location.href = "/Lab1Filter/forAdminPage?txtSearchItem=&txtMin=&txtMax=&cboCategory=NULL&btAction=SearchItem";
}

function goBack() {
    window.history.back();
}

function ConfirmFunction() {
    var r = confirm("Choose yes if you want to delete!");
    if (r == true) {
        return noCheck();
    } else {
        return false;
    }
}

function noCheck() {
    if (document.querySelectorAll('input[type="checkbox"]:checked').length == 0) {
        alert("You haven't check any row");
        return false;
    }
}

function fileValidation() {
    var fileInput = document.getElementById('fileimage');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
    if (!allowedExtensions.exec(filePath)) {
        alert('Invalid image type');
        fileInput.value = '';
        return false;
    }
}

function checkNumFunction() {
  var x, text;
  x = document.getElementById("quantityNum").value;
  if (isNaN(x) || x < 1 || x > 99999999999) {
    alert('Not a number');
  }
}