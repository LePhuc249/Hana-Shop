<%-- 
    Document   : viewHistory
    Created on : Mar 10, 2021, 2:45:36 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View History Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forAdminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with customer account !!!!!
                    </font>
                </h1>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <c:set var="CurrentAccountStatus" value="${sessionScope.STATUSACCOUNT}"/>
                <c:set var="errorROLE" value="${requestScope.ERRORROLE}"/>
                <c:set var="errorDeleteOrder" value="${requestScope.ERRORDELETEORDER}"/>
                <c:set var="errorStatus" value="${requestScope.STATUSFEEDBACKERROR}"/>
                <c:set var="errorNewStatus" value="${requestScope.NEWACCOUNTSESSION}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="viewCartPage">View Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                    <c:if test="${not empty errorDeleteOrder}">
                        <font color="red">
                        <br/>${errorDeleteOrder}
                        </font>
                    </c:if>
                    <c:if test="${not empty errorStatus}">
                        <font color="red">
                        <br/>${errorStatus}
                        </font>
                    </c:if>
                    <c:if test="${not empty errorNewStatus}">
                        <font color="red">
                        ${errorNewStatus}
                        </font>
                    </c:if>
                </div>
                <c:if test="${CurrentAccountStatus eq 'Active'}">
                    <div>
                        <h2>View shopping history</h2>
                        <form action="searchHistory" method="POST">
                            Input shopping date:<input type="text" name="txtShoppingDate" value="${param.txtShoppingDate}" placeholder="YYYY-MM-DD"/>
                            Input name:<input type="text" name="txtSearchName" value="${param.txtSearchName}"/><br/>
                            <input type="submit" value="SearchHistoryBuy" name="btAction" />
                        </form>
                        <c:set var="searchDateValue" value="${param.txtShoppingDate}"/>
                        <c:set var="searchName" value="${param.txtSearchName}"/>
                        <c:if test="${not empty searchDateValue or not empty searchName}">
                            <c:set var="result" value="${requestScope.HISTORYUSERORDER}"/>
                            <c:if test="${not empty result}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Order Date</th>
                                            <th>Date Delivery</th>
                                            <th>Payment Method</th>
                                            <th>Status</th>
                                            <th>Total Price</th>
                                            <th>Discount value</th>
                                            <th>Detail</th>
                                            <th>Delete Order</th>
                                            <th>Feedback</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:set var="listCouponsDB" value="${sessionScope.LISTCOUPONSTOCHECK}"/>
                                        <c:forEach var="dto" items="${result}" varStatus="counter">
                                        <form action="transfer" method="GET">
                                            <tr>
                                                <td>
                                                    ${counter.count}
                                                    <input type="hidden" name="txtOrderID" value="${dto.key.orderID}"/>
                                                </td>
                                                <td>
                                                    ${dto.key.createDate}  
                                                </td>
                                                <td>
                                                    ${dto.key.dateDelivery}  
                                                </td>
                                                <td>
                                                    <c:if test="${dto.key.paymentMehtod eq 'COD'}">
                                                        Pay on Shipping
                                                    </c:if>
                                                    <c:if test="${dto.key.paymentMehtod eq 'ITB'}">
                                                        Internet Banking
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <c:if test="${dto.key.status eq 'Ca'}">
                                                        Cancel
                                                    </c:if>
                                                    <c:if test="${dto.key.status eq 'Co'}">
                                                        Confirm
                                                    </c:if>
                                                    <c:if test="${dto.key.status eq 'Fi'}">
                                                        Finish
                                                    </c:if>
                                                    <c:if test="${dto.key.status eq 'Pr'}">
                                                        Process
                                                    </c:if>
                                                </td>
                                                <td>
                                                    ${dto.key.totalPrice}
                                                </td>
                                                <td>
                                                    <c:if test="${not empty dto.key.couponID}">
                                                        <c:forEach var="coupon" items="${listCouponsDB}" varStatus="counter">
                                                            <c:if test="${dto.key.couponID eq coupon.couponsID}">
                                                                ${coupon.discountAmount}%
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                    <c:if test="${empty dto.key.couponID}">
                                                        <font color="red">
                                                        None
                                                        </font>
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <table border="1">
                                                        <tbody>
                                                            <c:forEach var="orderDetail" items="${dto.value}" varStatus="counter">
                                                                <c:if test="${dto.key.orderID eq orderDetail.orderID}">
                                                                    <tr>
                                                                        <td>
                                                                            ${orderDetail.itemProperty}
                                                                        </td>
                                                                    </tr>
                                                                </c:if>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <c:if test="${dto.key.status eq 'Ca'}">
                                                        <font color="red">
                                                        Have been cancel
                                                        </font>
                                                    </c:if>
                                                    <c:if test="${dto.key.status eq 'Co'}">
                                                        <input type="hidden" name="valueSearchName" value="${searchName}"/>
                                                        <input type="hidden" name="valueSearchDate" value="${searchDateValue}"/>
                                                        <input type="submit" name="btAction" value="Delete Order"/>
                                                    </c:if>
                                                    <c:if test="${dto.key.status eq 'Pr'}">
                                                        <input type="hidden" name="valueSearchName" value="${searchName}"/>
                                                        <input type="hidden" name="valueSearchDate" value="${searchDateValue}"/>
                                                        <input type="submit" name="btAction" value="Delete Order"/>
                                                    </c:if>
                                                    <c:if test="${dto.key.status eq 'Fi'}">
                                                        <font color="red">
                                                        Can't delete
                                                        </font>
                                                    </c:if>
                                                </td>
                                                <td>
                                                    <input type="submit" name="btAction" value="Feedback"/>
                                                </td>
                                            </tr>
                                        </form>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>
                            <c:if test="${empty result}">
                                <h1>
                                    <font color="red">
                                    No record match
                                    </font>
                                </h1>
                            </c:if>
                        </c:if>    
                    </div>
                </c:if>
                <c:if test="${CurrentAccountStatus eq 'New'}">
                    <h1>
                        <font color="red">
                        Account haven't active.Please active account!
                        </font>
                    </h1>
                </c:if>

            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="homePage">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="viewCartPage">View Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="showHistoryPage">History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with customer account!!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
