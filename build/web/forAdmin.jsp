<%-- 
    Document   : forAdmin
    Created on : Mar 10, 2021, 12:46:52 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <c:set var="errorROLE" value="${requestScope.errorROLE}"/>
                <c:set var="errorDelete" value="${requestScope.ERRORDELETE}"/>
                <c:set var="searchNoOfPage" value="${sessionScope.NOOFPAGEFORADMIN}"/>
                <c:set var="searchCurrentPage" value="${sessionScope.CURRENTPAGEFORADMIN}"/>
                <c:set var="listCate" value="${applicationScope.CATEGORYLIST}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forAdminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                    <c:if test="${not empty errorDelete}">
                        <font color="red">
                        <br/>${errorDelete}
                        </font>
                    </c:if>
                </div>
                <div>
                    <c:if test="${not empty listCate}">
                        <div class="label-box center-me2">
                            <h2>Search items page</h2>
                        </div>
                        <div class="search-box center-me2">
                            <form action="searchAdmin" class="register-form">
                                Item name: <input type="text" name="txtSearchItem" value="${param.txtSearchItem}"/>
                                Range of Money:<input type="text" name="txtMin" value="${param.txtMin}" style="width:60px;"/>-<input type="text" name="txtMax" value="${param.txtMax}" style="width:60px;"/> 
                                <select name="cboCategory">
                                    <option value="NULL">-Category-</option>
                                    <c:forEach var="category" items="${listCate}">
                                        <option value="${category.categoryID}" <c:if test="${param.cboCategory eq category.categoryID}">selected="true"</c:if>>${category.categoryName}</option>
                                    </c:forEach>
                                </select>
                                <input type="submit" value="SearchItem" name="btAction" />
                            </form>
                            <c:if test="${not empty errorDate}">
                                <h2>
                                    <font color="red">
                                    ${errorDate}
                                    </font>
                                </h2>
                            </c:if>
                        </div>
                        <c:set var="searchValue" value="${param.txtSearchItem}"/>
                        <c:set var="searchCate" value="${param.cboCategory}"/>
                        <c:set var="searchMin" value="${param.txtMin}"/>
                        <c:set var="searchMax" value="${param.txtMax}"/>
                        <c:choose>
                            <c:when test="${not empty searchValue or not empty searchCate or (not empty searchMin and not empty searchMax)}">
                                <c:set var="listItems" value="${requestScope.SEARCHITEMRESULTFORADMIN}"/>
                                <div class="table-box center-me2">
                                    <c:if test="${not empty listItems}">
                                        <form action="deleteItem" method="POST" class="register-form">
                                            <table border="1">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Name</th>
                                                        <th>Image</th>
                                                        <th>Description</th>
                                                        <th>Price</th>
                                                        <th>Quantity</th>
                                                        <th>Status</th>
                                                        <th>Category</th>
                                                        <th>Delete</th>
                                                        <th>Update</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach var="items" items="${listItems}" varStatus="counter">
                                                        <tr>
                                                            <td>
                                                                ${counter.count}
                                                            </td>
                                                            <td>
                                                                ${items.itemName}
                                                            </td>
                                                            <td>
                                                                <img alt="itemImage" src="${items.img}" width="100" height="100"/>
                                                            </td>
                                                            <td>
                                                                ${items.description}
                                                            </td>
                                                            <td>
                                                                ${items.price}$
                                                            </td>
                                                            <td>
                                                                ${items.quantity}
                                                            </td>
                                                            <td>
                                                                ${items.status}
                                                                <c:set var="itemStatus" value="${items.status}"/>
                                                            </td>
                                                            <td>
                                                                ${items.categoryID}
                                                            </td>
                                                            <td>
                                                                <c:if test="${itemStatus eq 'Active'}">
                                                                    <input type="hidden" name="page" value="${searchCurrentPage}"/>
                                                                    <input type="hidden" name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                    <input type="hidden" name="txtMin" value="${param.txtMin}"/>
                                                                    <input type="hidden" name="txtMax" value="${param.txtMax}"/>
                                                                    <input type="hidden" name="cboCategory" value="${param.cboCategory}"/>
                                                                    <input type="checkbox" name="valueDelete" value="${items.itemName}"/>
                                                                </c:if>
                                                                <c:if test="${itemStatus eq 'Inactive'}">
                                                                    <font color="red">
                                                                    x
                                                                    </font>
                                                                </c:if>
                                                            </td>
                                                            <td>
                                                                <c:url var="urlRewriting" value="transfer">
                                                                    <c:param name="btAction" value="Update"/>
                                                                    <c:param name="id" value="${items.itemID}"/>
                                                                    <c:param name="searchValue" value="${param.txtSearchItem}"/>
                                                                    <c:param name="searchCate" value="${param.cboCategory}"/>
                                                                    <c:param name="searchMin" value="${param.txtMin}"/>
                                                                    <c:param name="searchMax" value="${param.txtMax}"/>
                                                                    <c:param name="page" value="${param.page}"/>
                                                                </c:url>
                                                                <a href="${urlRewriting}">Update</a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                            <c:if test="${not empty searchNoOfPage}">
                                                <table border="1" cellpadding="5" cellspacing="5">
                                                    <tr>
                                                        <c:if test="${searchCurrentPage!=1}">
                                                            <c:url var="prevLink" value="SearchForAdmin">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${searchCurrentPage-1}"/>
                                                                <c:param name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                <c:param name="txtMin" value="${param.txtMin}"/>
                                                                <c:param name="txtMax" value="${param.txtMax}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            </c:url>
                                                            <td><a href="${prevLink}">Previous</a></td>
                                                        </c:if>
                                                        <c:forEach begin="1" end="${searchNoOfPage}" var="i">
                                                            <c:choose>
                                                                <c:when test="${searchCurrentPage eq i}">
                                                                    <td>${i}</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url var="pageLink" value="SearchForAdmin">
                                                                        <c:param name="btAction" value="SearchItem"/>
                                                                        <c:param name="page" value="${i}"/>
                                                                        <c:param name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                        <c:param name="txtMin" value="${param.txtMin}"/>
                                                                        <c:param name="txtMax" value="${param.txtMax}"/>
                                                                        <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                    </c:url>
                                                                    <td>
                                                                        <a href="${pageLink}">${i}</a>
                                                                    </td>
                                                                </c:otherwise>    
                                                            </c:choose>
                                                        </c:forEach>
                                                        <c:if test="${searchCurrentPage < searchNoOfPage}">
                                                            <c:url var="nextLink" value="SearchForAdmin">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${searchCurrentPage+1}"/>
                                                                <c:param name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                <c:param name="txtMin" value="${param.txtMin}"/>
                                                                <c:param name="txtMax" value="${param.txtMax}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            </c:url>
                                                            <td><a href="${nextLink}">Next</a></td>
                                                        </c:if>            
                                                    </tr>
                                                </table>
                                            </c:if>
                                            <input type="hidden" name="txtPage" value="${param.page}"/>
                                            <input type="submit" class="btn btn-danger" value="DeleteItem" name="btAction" onclick="return ConfirmFunction()"/>
                                        </form>
                                    </c:if>
                                    <c:if test="${empty listItems}">
                                        <h3>
                                            <font color="red">
                                            No record matches
                                            </font>
                                        </h3>
                                    </c:if>
                                </div>
                                <div class="create-button">
                                    <input type="submit" class="btn btn-success" value="CreateItemForm" label="Create Item" name="btAction" onclick="goToCreateItemPage()"/>
                                </div>
                            </c:when>
                            <c:when test="${empty searchValue and empty searchMin and empty searchMax and empty searchCate}">
                                <c:set var="listAllItems" value="${applicationScope.ITEMLISTFORADMIN}"/>
                                <div class="table-box center-me2">
                                    <c:if test="${not empty listAllItems}">
                                        <form action="deleteItem" method="POST" class="register-form">
                                            <table border="1">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Name</th>
                                                        <th>Image</th>
                                                        <th>Description</th>
                                                        <th>Price</th>
                                                        <th>Quantity</th>
                                                        <th>Status</th>
                                                        <th>Category</th>
                                                        <th>Delete</th>
                                                        <th>Update</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach var="items" items="${listAllItems}" varStatus="counter">
                                                        <tr>
                                                            <td>
                                                                ${counter.count}
                                                            </td>
                                                            <td>
                                                                ${items.itemName}
                                                            </td>
                                                            <td>
                                                                <img alt="itemImage" src="${items.img}" width="100" height="100"/>
                                                            </td>
                                                            <td>
                                                                ${items.description}
                                                            </td>
                                                            <td>
                                                                ${items.price}$
                                                            </td>
                                                            <td>
                                                                ${items.quantity}
                                                            </td>
                                                            <td>
                                                                ${items.status}
                                                                <c:set var="itemStatus" value="${items.status}"/>

                                                            </td>
                                                            <td>
                                                                ${items.categoryID}
                                                            </td>
                                                            <td>
                                                                <c:if test="${itemStatus eq 'Active'}">
                                                                    <input type="hidden" name="page" value="${searchCurrentPage}"/>
                                                                    <input type="hidden" name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                    <input type="hidden" name="txtMin" value="${param.txtMin}"/>
                                                                    <input type="hidden" name="txtMax" value="${param.txtMax}"/>
                                                                    <input type="hidden" name="cboCategory" value="${param.cboCategory}"/>
                                                                    <input type="checkbox" name="valueDelete" value="${items.itemName}"/>
                                                                </c:if>
                                                                <c:if test="${itemStatus eq 'Inactive'}">
                                                                    <font color="red">
                                                                    x
                                                                    </font>
                                                                </c:if>
                                                            </td>
                                                            <td>
                                                                <c:url var="urlRewriting" value="transfer">
                                                                    <c:param name="btAction" value="Update"/>
                                                                    <c:param name="id" value="${items.itemID}"/>
                                                                    <c:param name="searchValue" value="${param.txtSearchItem}"/>
                                                                    <c:param name="searchCate" value="${param.cboCategory}"/>
                                                                    <c:param name="searchMin" value="${param.txtMin}"/>
                                                                    <c:param name="searchMax" value="${param.txtMax}"/>
                                                                    <c:param name="page" value="${param.page}"/>
                                                                </c:url>
                                                                <a href="${urlRewriting}">Update</a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                            <c:if test="${not empty searchNoOfPage}">
                                                <table border="1" cellpadding="5" cellspacing="5">
                                                    <tr>
                                                        <c:if test="${searchCurrentPage!=1}">
                                                            <c:url var="prevLink" value="SearchForAdmin">
                                                                <c:param name="btAction" value="SearchItem"/>
                                                                <c:param name="page" value="${searchCurrentPage-1}"/>
                                                                <c:param name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                <c:param name="txtMin" value="${param.txtMin}"/>
                                                                <c:param name="txtMax" value="${param.txtMax}"/>
                                                                <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                            </c:url>
                                                            <td><a href="${prevLink}">Previous</a></td>
                                                        </c:if>
                                                        <c:forEach begin="1" end="${searchNoOfPage}" var="i">
                                                            <c:choose>
                                                                <c:when test="${searchCurrentPage eq i}">
                                                                    <td>${i}</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url var="pageLink" value="SearchForAdmin">
                                                                        <c:param name="btAction" value="SearchItem"/>
                                                                        <c:param name="page" value="${i}"/>
                                                                        <c:param name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                        <c:param name="txtMin" value="${param.txtMin}"/>
                                                                        <c:param name="txtMax" value="${param.txtMax}"/>
                                                                        <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                    </c:url>
                                                                    <td><a href="${pageLink}">${i}</a></td>
                                                                    </c:otherwise>    
                                                                </c:choose>
                                                            </c:forEach>
                                                            <c:if test="${searchCurrentPage < searchNoOfPage}">
                                                                <c:url var="nextLink" value="SearchForAdmin">
                                                                    <c:param name="btAction" value="SearchItem"/>
                                                                    <c:param name="page" value="${searchCurrentPage+1}"/>
                                                                    <c:param name="txtSearchItem" value="${param.txtSearchItem}"/>
                                                                    <c:param name="txtMin" value="${param.txtMin}"/>
                                                                    <c:param name="txtMax" value="${param.txtMax}"/>
                                                                    <c:param name="cboCategory" value="${param.cboCategory}"/>
                                                                </c:url>
                                                            <td><a href="${nextLink}">Next</a></td>
                                                        </c:if>            
                                                    </tr>
                                                </table>
                                            </c:if>
                                            <input type="hidden" name="txtPage" value="${param.page}"/>
                                            <input type="submit" class="btn btn-danger" value="DeleteItem" name="btAction" onclick="return ConfirmFunction()"/>
                                        </form>
                                    </c:if>
                                </div>
                                <div class="create-button">
                                    <input type="submit" class="btn btn-success" value="CreateItemForm" label="Create Item" name="btAction" onclick="goToCreateItemPage()"/>
                                </div>
                            </c:when>
                        </c:choose>
                    </c:if>
                    <c:if test="${empty listCate}">
                        <h1 class="empty-category">There are nothing in list category </h1>
                    </c:if>
                </div>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="viewCartPage">View Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with admin account !!!!!
                    </font>
                </h1>
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="homePage">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="viewCartPage">View Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="showHistoryPage">History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with admin account!!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
