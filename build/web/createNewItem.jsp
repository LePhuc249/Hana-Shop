<%-- 
    Document   : createNewItem
    Created on : Mar 10, 2021, 2:24:36 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create New Item Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <c:set var="errorROLE" value="${requestScope.ERRORROLE}"/>
                <c:set var="errors" value="${requestScope.ERRORCREATEITEM}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forAdminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                </div>
                <div class="container-fluid bg">
                    <div class="row justify-content-center">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <form action="createItem" method="post" class="register-form" enctype="multipart/form-data">
                                <h1>Create New Item</h1>
                                <input type="hidden" name="txtItemID" value="${updateItem.itemID}"/>
                                <div class="form-group">
                                    Item name:
                                    <input type="text" class="form-control" name="txtItemname" placeholder="Enter Item name" value="${param.txtItemname}"/>
                                    <c:if test="${not empty errors.nameLengthError}">
                                        <font color="red">
                                        ${errors.nameLengthError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Item image:
                                    <input type="file" class="form-control" id="fileimage" name="photoItem" placeholder="Enter Item name" accept="image/*" value="${param.photoItem}" onchange="fileValidation()"/>
                                    <c:if test="${not empty errors.imageError}">
                                        <font color="red">
                                        ${errors.imageError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Item description:
                                    <input type="text" class="form-control" name="txtItemdescription" placeholder="Enter Item description" value="${param.txtItemdescription}"/>
                                    <c:if test="${not empty errors.descriptionLengthError}">
                                        <font color="red">
                                        ${errors.descriptionLengthError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Item price:
                                    <input type="text" class="form-control" name="txtItemprice" placeholder="Enter Item price" value="${param.txtItemprice}"/>
                                    <c:if test="${not empty errors.priceError}">
                                        <font color="red">
                                        ${errors.priceError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Item product name:
                                    <input type="text" class="form-control" name="txtItemProductName" placeholder="Enter Item product name" value="${param.txtItemProductName}"/>
                                    <c:if test="${not empty errors.productNameLengthError}">
                                        <font color="red">
                                        ${errors.productNameLengthError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Item quantity:
                                    <input type="text" class="form-control" name="txtItemquantity" placeholder="Enter Item quantity" value="${param.txtItemquantity}"/>
                                    <c:if test="${not empty errors.quantityError}">
                                        <font color="red">
                                        ${errors.quantityError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <div class="form-group">
                                    Item category:
                                    <select name="cboCategory">
                                        <c:if test="${not empty applicationScope.CATEGORYLIST}">
                                            <c:forEach var="category" items="${applicationScope.CATEGORYLIST}">
                                                <option value="${category.categoryID}">${category.categoryName}</option>
                                            </c:forEach>
                                        </c:if>
                                    </select>
                                    <c:if test="${not empty errors.categoryError}">
                                        <font color="red">
                                        ${errors.categoryError}
                                        </font><br/>
                                    </c:if>
                                </div>
                                <input type="submit" name="btAction" value="CreateItem"/>
                            </form>
                        </div>
                    </div>
                </div>	
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="viewCartPage">View Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with admin account !!!!!
                    </font>
                </h1>
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="homePage">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="viewCartPage">View Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="showHistoryPage">History</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with admin account !!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
