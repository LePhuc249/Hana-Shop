<%-- 
    Document   : feedback
    Created on : Mar 10, 2021, 3:24:49 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Feedback Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.4/css/all.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <script src="js/index.js"></script>
    </head>
    <body>
        <c:set var="dtoAccount" value="${sessionScope.ACCOUNTOBJECT}"/>
        <c:set var="roleAccount" value="${sessionScope.ROLENAME}"/>
        <c:if test="${not empty dtoAccount}">
            <c:if test="${roleAccount eq 'Admin'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forAdminPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                </div>
                <h1>
                    <font color="red">
                    Please go to logout and login with customer account !!!!!
                    </font>
                </h1>
            </c:if>
            <c:if test="${roleAccount eq 'Customer'}">
                <c:set var="fullname" value="${dtoAccount.fullname}"/>
                <c:set var="CurrentAccountStatus" value="${sessionScope.STATUSACCOUNT}"/>
                <c:set var="errorROLE" value="${requestScope.ERRORROLE}"/>
                <c:set var="errorSendFeedback" value="${requestScope.ERRORSENDFEEDBACK}"/>
                <c:set var="errorNewStatus" value="${requestScope.NEWACCOUNTSESSION}"/>
                <div>
                    <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-branch" href="#">
                                <img src= "img/Logo.png" height = "100" alt="#">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                            </a>
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="forCustomerPage">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="viewCartPage">View Cart</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="showHistoryPage">History</a>
                                    </li>
                                    <li class="nav-item">
                                        <form action="logout" method="GET">
                                            <input class="nav-link btn btn-danger" type="submit" value="Logout" name="btAction" />
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div>
                    <font color="red">
                    Welcome, ${fullname}
                    </font>
                    <c:if test="${not empty errorROLE}">
                        <font color="red">
                        <br/>${errorROLE}
                        </font>
                    </c:if>
                    <c:if test="${not empty errorSendFeedback}">
                        <font color="red">
                        <br/>${errorSendFeedback}
                        </font>
                    </c:if>
                    <c:if test="${not empty errorNewStatus}">
                        <font color="red">
                        ${errorNewStatus}
                        </font>
                    </c:if>
                </div>
                <c:if test="${CurrentAccountStatus eq 'Active'}">
                    <div>
                        <c:set var="feedbackDTO" value="${sessionScope.FEEDBACKEXIST}"/>
                        <c:set var="orderDTO" value="${sessionScope.ORDER}"/>
                        <c:set var="listCoupons" value="${sessionScope.COUPONSLIST}"/>
                        <c:set var="listDetail" value="${sessionScope.ORDERDETAILLIST}"/>
                        <c:if test="${empty feedbackDTO}">
                            Order Information<br/>
                            <c:if test="${not empty orderDTO}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Date Delivery</th>
                                            <th>Payment Method</th>
                                            <th>Status</th>
                                            <th>Total Price</th>
                                            <th>Coupon Code</th>
                                            <th>Discount Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                ${orderDTO.createDate}
                                            </td>
                                            <td>
                                                ${orderDTO.dateDelivery}
                                            </td>
                                            <td>
                                                ${orderDTO.paymentMehtod}
                                            </td>
                                            <td>
                                                ${orderDTO.status}
                                            </td>
                                            <td>
                                                ${orderDTO.totalPrice}
                                            </td>
                                            <td>
                                                <c:forEach var="coupon" items="${listCoupons}" varStatus="counter">
                                                    <c:if test="${orderDTO.couponID eq coupon.couponsID}">
                                                        ${coupon.couponsCode}
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                            <td>
                                                <c:forEach var="coupon" items="${listCoupons}" varStatus="counter">
                                                    <c:if test="${orderDTO.couponID eq coupon.couponsID}">
                                                        ${coupon.discountAmount}%
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </c:if>
                            Order Details<br/>
                            <c:if test="${not empty listDetail}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="list" items="${listDetail}" varStatus="counter">
                                            <tr>
                                                <td>
                                                    ${list.itemProperty}
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>
                            <c:if test="${not empty orderDTO.orderID}">
                                <form action="submitFeeback" method="POST">
                                    <input type="hidden" name="txtOrderID" value="${orderDTO.orderID}"/>
                                    Comment: <textarea type="textarea" name="txtComment" value="" maxlength="100"></textarea><br/>
                                    Score: <input type="text" name="txtNumberQuality" value=""/>/10<br/>
                                    <input type="submit" name="btAction" value="Submit feedback"/>
                                </form>
                            </c:if>
                        </c:if>
                        <c:if test="${not empty feedbackDTO}">
                            Order Information<br/>
                            <c:if test="${not empty orderDTO}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>Order Date</th>
                                            <th>Date Delivery</th>
                                            <th>Payment Method</th>
                                            <th>Status</th>
                                            <th>Total Price</th>
                                            <th>Coupon Code</th>
                                            <th>Discount Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                ${orderDTO.createDate}
                                            </td>
                                            <td>
                                                ${orderDTO.dateDelivery}
                                            </td>
                                            <td>
                                                ${orderDTO.paymentMehtod}
                                            </td>
                                            <td>
                                                ${orderDTO.status}
                                            </td>
                                            <td>
                                                ${orderDTO.totalPrice}
                                            </td>
                                            <td>
                                                <c:forEach var="coupon" items="${listCoupons}" varStatus="counter">
                                                    <c:if test="${orderDTO.couponID eq coupon.couponsID}">
                                                        ${coupon.couponsCode}
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                            <td>
                                                <c:forEach var="coupon" items="${listCoupons}" varStatus="counter">
                                                    <c:if test="${orderDTO.couponID eq coupon.couponsID}">
                                                        ${coupon.discountAmount}%
                                                    </c:if>
                                                </c:forEach>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </c:if>
                            Order Details<br/>
                            <c:if test="${not empty listDetail}">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="list" items="${listDetail}" varStatus="counter">
                                            <tr>
                                                <td>
                                                    ${list.itemProperty}
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </c:if>
                            Feedback Content: ${feedbackDTO.feedbackContent}<br/>
                            Number of Quality: ${feedbackDTO.counter}/10<br/>
                        </c:if>
                    </div>
                </c:if>
                <c:if test="${CurrentAccountStatus eq 'New'}">
                    <h1>
                        <font color="red">
                        Account haven't active.Please active account!
                        </font>
                    </h1>
                </c:if>    
            </c:if>
        </c:if>
        <c:if test="${empty dtoAccount}">
            <div>
                <nav class = "navbar navbar-expand-md navbar-light bg-light sticky-top">
                    <div class="container-fluid">
                        <a class="navbar-branch" href="#">
                            <img src= "img/Logo.png" height = "100" alt="#">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </a>
                        <div class="collapse navbar-collapse" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link" href="homePage">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="viewCartPage">View Cart</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="showHistoryPage">History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="loginPage">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <h1>
                <font color="red">
                Please go to login page and login with customer account!!!!!
                </font>
            </h1>
            <button type="button" onclick="goToLoginPage()">Click here return the Login Page</button><br/>
        </c:if>
    </body>
</html>
